<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Blank Page</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?= base_url("assets/adminlte") ?>/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?= base_url("assets/adminlte") ?>/plugins/select2/select2.min.css">

  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url("assets/adminlte") ?>/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?= base_url("assets/adminlte") ?>/dist/css/skins/_all-skins.min.css">
  <!-- custom -->
  <link rel="stylesheet" href="<?= base_url("assets/adminlte") ?>/dist/css/custom.css">


  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>

<body class="hold-transition skin-blue sidebar-mini">
  <!-- Site wrapper -->
  <div class="wrapper">

    <?php
    require_once("incld/header_incld.php");
    require_once("incld/menu_incld.php");
    ?>

    <!-- =============================================== -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->

      <div class="row">
        <div class="col-md-4">
          <section class="content-header">
            <h1>Outlets</h1>
            <ol class="breadcrumb horizontal">
              <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
              <li class="active">outlets</li>
            </ol>
          </section>
        </div>
        <div class="col-md-8">
          <ol class="right_btn">
            <li>
              <div class="input-group" style="width:200px">
                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                <input type="text" class="form-control" placeholder="Search Outlets..">
              </div>
            </li>
          
            <li><button  data-toggle="modal" data-target="#newElement" class="btn btn-block btn-primary"><i class="fa fa-fw fa-plus"></i> New Outlet</button></li>
          </ol>
        </div>
      </div>

        <!-- Modal -->
        <div class="modal fade" id="newElement" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabel">New Outlet</h4>
            </div>
            <div class="modal-body">
              <form>
                <div class="row">

                
                  <div class="col-md-6">
                    <div class="form-group required">
                      <label>Outlet Name</label>
                      <input type="text" class="form-control" placeholder="Enter Name">
                      <span class="help-block">This is required</span>
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group required">
                      <label>Phone No.</label>
                      <input type="text" class="form-control" placeholder="Enter Name">
                      <span class="help-block">This is required</span>
                    </div>
                  </div>

                  <div class="col-md-12">
                    <div class="form-group required">
                      <label>Address</label>
                      <textarea class="form-control" rows="3" placeholder="Address"></textarea>
                      <span class="help-block">This is required</span>
                    </div>
                  </div>



                </div>
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary">Save</button>
            </div>
          </div>
        </div>
      </div>
      <!-- Main content -->
      <section class="content">
        <div class="row">

        <div class="col-md-4">
          <!-- Widget: user widget style 1 -->
          <div class="box box-widget widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-red">
              <div class="widget-user-image">
                <img class="img-circle" src="http://localhost/ci/assets/adminlte/dist/img/restaurent-icon.jpg" alt="User Avatar">
              </div>
              <!-- /.widget-user-image -->
              <h3 class="widget-user-username">Outlet Name</h3>
              <h5 class="widget-user-desc">Code : PR00098</h5>
            </div>
            <div class="box-footer">
              <ul class="nav outlet-details">
                <li><strong>Phone No.:</strong> 9864678888 </li>
                <li><strong>Started Date:</strong> 04/07/2019 </li>
                <li><strong>Address:</strong> <br>Lorem Ipsum is simply dummy text. </li>
                <li>
                  <button type="button" class="btn btn-primary btn-xs">Edit</button>
                  <button type="button" class="btn btn-danger btn-xs">Delete</button>
                </li>
              </ul>
            </div>
          </div>
        </div>
         <!-- /.widget-user -->

         <div class="col-md-4">
          <!-- Widget: user widget style 1 -->
          <div class="box box-widget widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-yellow">
              <div class="widget-user-image">
                <img class="img-circle" src="http://localhost/ci/assets/adminlte/dist/img/restaurent-icon.jpg" alt="User Avatar">
              </div>
              <!-- /.widget-user-image -->
              <h3 class="widget-user-username">Outlet Name</h3>
              <h5 class="widget-user-desc">Code : PR00098</h5>
            </div>
            <div class="box-footer">
              <ul class="nav outlet-details">
                <li><strong>Phone No.:</strong> 9864678888 </li>
                <li><strong>Started Date:</strong> 04/07/2019 </li>
                <li><strong>Address:</strong> <br>Lorem Ipsum is simply dummy text. </li>
                <li>
                  <button type="button" class="btn btn-primary btn-xs">Edit</button>
                  <button type="button" class="btn btn-danger btn-xs">Delete</button>
                </li>
              </ul>
            </div>
          </div>
        </div>
         <!-- /.widget-user -->

         <div class="col-md-4">
          <!-- Widget: user widget style 1 -->
          <div class="box box-widget widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-aqua">
              <div class="widget-user-image">
                <img class="img-circle" src="http://localhost/ci/assets/adminlte/dist/img/restaurent-icon.jpg" alt="User Avatar">
              </div>
              <!-- /.widget-user-image -->
              <h3 class="widget-user-username">Outlet Name</h3>
              <h5 class="widget-user-desc">Code : PR00098</h5>
            </div>
            <div class="box-footer">
              <ul class="nav outlet-details">
                <li><strong>Phone No.:</strong> 9864678888 </li>
                <li><strong>Started Date:</strong> 04/07/2019 </li>
                <li><strong>Address:</strong> <br>Lorem Ipsum is simply dummy text. </li>
                <li>
                  <button type="button" class="btn btn-primary btn-xs">Edit</button>
                  <button type="button" class="btn btn-danger btn-xs">Delete</button>
                </li>
              </ul>
            </div>
          </div>
        </div>
         <!-- /.widget-user -->

         <div class="col-md-4">
          <!-- Widget: user widget style 1 -->
          <div class="box box-widget widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-green">
              <div class="widget-user-image">
                <img class="img-circle" src="http://localhost/ci/assets/adminlte/dist/img/restaurent-icon.jpg" alt="User Avatar">
              </div>
              <!-- /.widget-user-image -->
              <h3 class="widget-user-username">Outlet Name</h3>
              <h5 class="widget-user-desc">Code : PR00098</h5>
            </div>
            <div class="box-footer">
              <ul class="nav outlet-details">
                <li><strong>Phone No.:</strong> 9864678888 </li>
                <li><strong>Started Date:</strong> 04/07/2019 </li>
                <li><strong>Address:</strong> <br>Lorem Ipsum is simply dummy text. </li>
                <li>
                  <button type="button" class="btn btn-primary btn-xs">Edit</button>
                  <button type="button" class="btn btn-danger btn-xs">Delete</button>
                </li>
              </ul>
            </div>
          </div>
        </div>
         <!-- /.widget-user -->

         <div class="col-md-4">
          <!-- Widget: user widget style 1 -->
          <div class="box box-widget widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-red">
              <div class="widget-user-image">
                <img class="img-circle" src="http://localhost/ci/assets/adminlte/dist/img/restaurent-icon.jpg" alt="User Avatar">
              </div>
              <!-- /.widget-user-image -->
              <h3 class="widget-user-username">Outlet Name</h3>
              <h5 class="widget-user-desc">Code : PR00098</h5>
            </div>
            <div class="box-footer">
              <ul class="nav outlet-details">
                <li><strong>Phone No.:</strong> 9864678888 </li>
                <li><strong>Started Date:</strong> 04/07/2019 </li>
                <li><strong>Address:</strong> <br>Lorem Ipsum is simply dummy text. </li>
              </ul>
            </div>
          </div>
        </div>
         <!-- /.widget-user -->

         <div class="col-md-4">
          <!-- Widget: user widget style 1 -->
          <div class="box box-widget widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-yellow">
              <div class="widget-user-image">
                <img class="img-circle" src="http://localhost/ci/assets/adminlte/dist/img/restaurent-icon.jpg" alt="User Avatar">
              </div>
              <!-- /.widget-user-image -->
              <h3 class="widget-user-username">Outlet Name</h3>
              <h5 class="widget-user-desc">Code : PR00098</h5>
            </div>
            <div class="box-footer">
              <ul class="nav outlet-details">
                <li><strong>Phone No.:</strong> 9864678888 </li>
                <li><strong>Started Date:</strong> 04/07/2019 </li>
                <li><strong>Address:</strong> <br>Lorem Ipsum is simply dummy text. </li>
                <li>
                  <button type="button" class="btn btn-primary btn-xs">Edit</button>
                  <button type="button" class="btn btn-danger btn-xs">Delete</button>
                </li>
              </ul>
            </div>
          </div>
        </div>
         <!-- /.widget-user -->

         <div class="col-md-4">
          <!-- Widget: user widget style 1 -->
          <div class="box box-widget widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-aqua">
              <div class="widget-user-image">
                <img class="img-circle" src="http://localhost/ci/assets/adminlte/dist/img/restaurent-icon.jpg" alt="User Avatar">
              </div>
              <!-- /.widget-user-image -->
              <h3 class="widget-user-username">Outlet Name</h3>
              <h5 class="widget-user-desc">Code : PR00098</h5>
            </div>
            <div class="box-footer">
              <ul class="nav outlet-details">
                <li><strong>Phone No.:</strong> 9864678888 </li>
                <li><strong>Started Date:</strong> 04/07/2019 </li>
                <li><strong>Address:</strong> <br>Lorem Ipsum is simply dummy text. </li>
                <li>
                  <button type="button" class="btn btn-primary btn-xs">Edit</button>
                  <button type="button" class="btn btn-danger btn-xs">Delete</button>
                </li>
              </ul>
            </div>
          </div>
        </div>
         <!-- /.widget-user -->

         <div class="col-md-4">
          <!-- Widget: user widget style 1 -->
          <div class="box box-widget widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-green">
              <div class="widget-user-image">
                <img class="img-circle" src="http://localhost/ci/assets/adminlte/dist/img/restaurent-icon.jpg" alt="User Avatar">
              </div>
              <!-- /.widget-user-image -->
              <h3 class="widget-user-username">Outlet Name</h3>
              <h5 class="widget-user-desc">Code : PR00098</h5>
            </div>
            <div class="box-footer">
              <ul class="nav outlet-details">
                <li><strong>Phone No.:</strong> 9864678888 </li>
                <li><strong>Started Date:</strong> 04/07/2019 </li>
                <li><strong>Address:</strong> <br>Lorem Ipsum is simply dummy text. </li>
                <li>
                  <button type="button" class="btn btn-primary btn-xs">Edit</button>
                  <button type="button" class="btn btn-danger btn-xs">Delete</button>
                </li>
              </ul>
            </div>
          </div>
        </div>
         <!-- /.widget-user -->

         <div class="col-md-4">
          <!-- Widget: user widget style 1 -->
          <div class="box box-widget widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-red">
              <div class="widget-user-image">
                <img class="img-circle" src="http://localhost/ci/assets/adminlte/dist/img/restaurent-icon.jpg" alt="User Avatar">
              </div>
              <!-- /.widget-user-image -->
              <h3 class="widget-user-username">Outlet Name</h3>
              <h5 class="widget-user-desc">Code : PR00098</h5>
            </div>
            <div class="box-footer">
              <ul class="nav outlet-details">
                <li><strong>Phone No.:</strong> 9864678888 </li>
                <li><strong>Started Date:</strong> 04/07/2019 </li>
                <li><strong>Address:</strong> <br>Lorem Ipsum is simply dummy text. </li>
                <li>
                  <button type="button" class="btn btn-primary btn-xs">Edit</button>
                  <button type="button" class="btn btn-danger btn-xs">Delete</button>
                </li>
              </ul>
            </div>
          </div>
        </div>
         <!-- /.widget-user -->

         <div class="col-md-4">
          <!-- Widget: user widget style 1 -->
          <div class="box box-widget widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-yellow">
              <div class="widget-user-image">
                <img class="img-circle" src="http://localhost/ci/assets/adminlte/dist/img/restaurent-icon.jpg" alt="User Avatar">
              </div>
              <!-- /.widget-user-image -->
              <h3 class="widget-user-username">Outlet Name</h3>
              <h5 class="widget-user-desc">Code : PR00098</h5>
            </div>
            <div class="box-footer">
              <ul class="nav outlet-details">
                <li><strong>Phone No.:</strong> 9864678888 </li>
                <li><strong>Started Date:</strong> 04/07/2019 </li>
                <li><strong>Address:</strong> <br>Lorem Ipsum is simply dummy text. </li>
                <li>
                  <button type="button" class="btn btn-primary btn-xs">Edit</button>
                  <button type="button" class="btn btn-danger btn-xs">Delete</button>
                </li>
              </ul>
            </div>
          </div>
        </div>
         <!-- /.widget-user -->

         <div class="col-md-4">
          <!-- Widget: user widget style 1 -->
          <div class="box box-widget widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-aqua">
              <div class="widget-user-image">
                <img class="img-circle" src="http://localhost/ci/assets/adminlte/dist/img/restaurent-icon.jpg" alt="User Avatar">
              </div>
              <!-- /.widget-user-image -->
              <h3 class="widget-user-username">Outlet Name</h3>
              <h5 class="widget-user-desc">Code : PR00098</h5>
            </div>
            <div class="box-footer">
              <ul class="nav outlet-details">
                <li><strong>Phone No.:</strong> 9864678888 </li>
                <li><strong>Started Date:</strong> 04/07/2019 </li>
                <li><strong>Address:</strong> <br>Lorem Ipsum is simply dummy text. </li>
                <li>
                  <button type="button" class="btn btn-primary btn-xs">Edit</button>
                  <button type="button" class="btn btn-danger btn-xs">Delete</button>
                </li>
              </ul>
            </div>
          </div>
        </div>
         <!-- /.widget-user -->

         <div class="col-md-4">
          <!-- Widget: user widget style 1 -->
          <div class="box box-widget widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-green">
              <div class="widget-user-image">
                <img class="img-circle" src="http://localhost/ci/assets/adminlte/dist/img/restaurent-icon.jpg" alt="User Avatar">
              </div>
              <!-- /.widget-user-image -->
              <h3 class="widget-user-username">Outlet Name</h3>
              <h5 class="widget-user-desc">Code : PR00098</h5>
            </div>
            <div class="box-footer">
              <ul class="nav outlet-details">
                <li><strong>Phone No.:</strong> 9864678888 </li>
                <li><strong>Started Date:</strong> 04/07/2019 </li>
                <li><strong>Address:</strong> <br>Lorem Ipsum is simply dummy text. </li>
                <li>
                  <button type="button" class="btn btn-primary btn-xs">Edit</button>
                  <button type="button" class="btn btn-danger btn-xs">Delete</button>
                </li>
              </ul>
            </div>
          </div>
        </div>
         <!-- /.widget-user -->

         

         
       
        </div>
        <!-- /.row -->
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <?php
    require_once("incld/footer_incld.php");
    require_once("incld/sidebar_incld.php");
    ?>
    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
  </div>
  <!-- ./wrapper -->




  <!-- jQuery 2.2.3 -->
  <script src="<?= base_url("assets/adminlte") ?>/plugins/jQuery/jquery-2.2.3.min.js"></script>
  <!-- Bootstrap 3.3.6 -->
  <script src="<?= base_url("assets/adminlte") ?>/bootstrap/js/bootstrap.min.js"></script>
  <!-- Select2 -->
  <script src="<?= base_url("assets/adminlte") ?>/plugins/select2/select2.full.min.js"></script>
  <!-- SlimScroll -->
  <script src="<?= base_url("assets/adminlte") ?>/plugins/slimScroll/jquery.slimscroll.min.js"></script>
  <!-- FastClick -->
  <script src="<?= base_url("assets/adminlte") ?>/plugins/fastclick/fastclick.js"></script>
  <!-- AdminLTE App -->
  <script src="<?= base_url("assets/adminlte") ?>/dist/js/app.min.js"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="<?= base_url("assets/adminlte") ?>/dist/js/demo.js"></script>
  <!-- page script -->
  <script>
    $(function() {

      $(".select2").select2({
        placeholder: "Category",
      });
    });
  </script>
</body>

</html>