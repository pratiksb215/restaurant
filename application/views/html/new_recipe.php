<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Blank Page</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?= base_url("assets/adminlte") ?>/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?= base_url("assets/adminlte") ?>/plugins/select2/select2.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="<?= base_url("assets/adminlte") ?>/plugins/iCheck/all.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url("assets/adminlte") ?>/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?= base_url("assets/adminlte") ?>/dist/css/skins/_all-skins.min.css">
  <!-- custom -->
  <link rel="stylesheet" href="<?= base_url("assets/adminlte") ?>/dist/css/custom.css">


  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>

<body class="hold-transition skin-blue sidebar-mini">
  <!-- Site wrapper -->
  <div class="wrapper">

    <?php
    require_once("incld/header_incld.php");
    require_once("incld/menu_incld.php");
    ?>

    <!-- =============================================== -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->

      <div class="row">
        <div class="col-md-4">
          <section class="content-header">
            <h1>New Recipe</h1>
            <ol class="breadcrumb horizontal">
              <li><a href="<?= base_url("html") ?>/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
              <li>Masters</li>
              <li><a href="<?= base_url("html") ?>/recipes">Recipes</a></li>
              <li class="active">New</li>
            </ol>
          </section>
        </div>
        <div class="col-md-8">
          <ol class="right_btn">
            <li><a href="<?= base_url("html") ?>/recipes" class="btn btn-block btn-primary"><i class="fa fa-fw fa-paper-plane-o"></i> Publish</a></li>
          </ol>
        </div>
      </div>

      <section class="content">
        <div class="row">
          <div class="col-md-6">
            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title"> Recipe Details</h3>
              </div>
              <div class="box-body">
                <div class="row">

                <div class="col-xs-12">
                  <div class="form-group btn-radio">
                <label>

                  <input type="radio" name="r2" class="minimal-red" checked>
                  Veg
                </label>
                <label>
                  <input type="radio" name="r2" class="minimal-red">
                  Non Veg
                </label>
              
              </div>
                  </div>
                  <div class="col-xs-12">
                    <div class="form-group required">
                      <label>Recipe Name</label>
                      <input type="text" class="form-control" placeholder="Recipe Name">
                      <span class="help-block">This is required</span>
                    </div>
                  </div>
                  <div class="col-xs-12">
                    <div class="form-group">

                      <select class="form-control select2" style="width: 100%;">
                        <option selected="selected">Select Category</option>
                        <option>Starter</option>
                        <option>Main Course</option>
                      </select>
                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div class="form-group">
                      <label>Description</label>
                      <textarea class="form-control" rows="3" placeholder="Enter ..."></textarea>
                    </div>
                  </div>

             

                  <div class="col-xs-5">
                    <img src="<?= base_url("assets/adminlte") ?>/dist/img/recipes/paneer-tikka.jpg" class="img-circle" alt="User Image">
                  </div>

                  <div class="col-xs-7">
                    <div class="form-group">
                      <label for="exampleInputFile">Image Upload</label>
                      <input type="file" id="exampleInputFile">
                      <p class="help-block">Example block-level help text here.</p>
                    </div>
                    <button width="50px" type="button" class="btn btn-primary">Upload</button>
                    <button width="50px" type="button" class="btn  btn-danger">Remove</button>
                  </div>


                </div>
              </div>
            </div>

          </div>

          <div class="col-md-6">
          <ul class="nav nav-tabs">
              <li class="active"><a href="#Ingredients" data-toggle="tab">Ingredients</a></li>
              <li><a href="#base" data-toggle="tab">Base</a></li>
           
            </ul>

            <div class="tab-content tab-slider">

<div class="tab-pane active" id="Ingredients">

            <div class="box box-widget">

              <div class="box-header">
                <h3 class="box-title"> Select Ingredients</h3>
              </div>

              <div class="box-body">


                <div class="repeater">
                  <div class="row repeater-row">
                    <div class="col-xs-5">
                      <div class="form-group">
                        <select class="form-control select2" style="width: 100%;">
                          <option selected="selected">Select Ingredients</option>
                          <option>Ingredients Name</option>
                          <option>Ingredients Name</option>
                        </select>
                      </div>
                    </div>

                    <div class="col-xs-2">
                      <div class="input-group" style="width:100%">
                        <input type="text" class="form-control" placeholder="Qty">
                      </div>
                    </div>

                    <div class="col-xs-3">
                      <div class="form-group">
                        <select class="form-control" style="width: 100%;">
                          <option selected="selected">Units</option>
                          <option>KG</option>
                          <option>G</option>
                        </select>
                      </div>
                    </div>

                    <div class="col-xs-2">
                      <button type="button" class="btn btn-default btn-xs add">
                        <i class="fa fa-plus"></i>
                      </button>
                      <button type="button" class="btn btn-default btn-xs remove">
                        <i class="fa fa-minus"></i>
                      </button>
                    </div>
                  </div>

                  <div class="row repeater-row">
                    <div class="col-xs-5">
                      <div class="form-group">
                        <select class="form-control select2" style="width: 100%;">
                          <option selected="selected">Select Ingredients</option>
                          <option>Ingredients Name</option>
                          <option>Ingredients Name</option>
                        </select>
                      </div>
                    </div>

                    <div class="col-xs-2">
                      <div class="input-group" style="width:100%">
                        <input type="text" class="form-control" placeholder="Qty">
                      </div>
                    </div>

                    <div class="col-xs-3">
                      <div class="form-group">
                        <select class="form-control" style="width: 100%;">
                          <option selected="selected">Units</option>
                          <option>KG</option>
                          <option>G</option>
                        </select>
                      </div>
                    </div>

                    <div class="col-xs-2">
                      <button type="button" class="btn btn-default btn-xs add">
                        <i class="fa fa-plus"></i>
                      </button>
                      <button type="button" class="btn btn-default btn-xs remove">
                        <i class="fa fa-minus"></i>
                      </button>
                    </div>
                  </div>

                  <div class="row repeater-row">
                    <div class="col-xs-5">
                      <div class="form-group">
                        <select class="form-control select2" style="width: 100%;">
                          <option selected="selected">Select Ingredients</option>
                          <option>Ingredients Name</option>
                          <option>Ingredients Name</option>
                        </select>
                      </div>
                    </div>

                    <div class="col-xs-2">
                      <div class="input-group" style="width:100%">
                        <input type="text" class="form-control" placeholder="Qty">
                      </div>
                    </div>

                    <div class="col-xs-3">
                      <div class="form-group">
                        <select class="form-control" style="width: 100%;">
                          <option selected="selected">Units</option>
                          <option>KG</option>
                          <option>G</option>
                        </select>
                      </div>
                    </div>

                    <div class="col-xs-2">
                      <button type="button" class="btn btn-default btn-xs add">
                        <i class="fa fa-plus"></i>
                      </button>
                      <button type="button" class="btn btn-default btn-xs remove">
                        <i class="fa fa-minus"></i>
                      </button>
                    </div>
                  </div>


                </div>

              </div>

            </div>
</div>

<div class="tab-pane" id="base">

            <div class="box box-widget">

              <div class="box-header">
                <h3 class="box-title"> Select Base</h3>
              </div>

              <div class="box-body">

              <div class="repeater">


                  <div class="row repeater-row">
                    <div class="col-xs-5">
                      <div class="form-group">
                        <select class="form-control select2" style="width: 100%;">
                          <option selected="selected">Select Base</option>
                          <option>Tomato Gravy	</option>
                          <option>Red Gravy	</option>
                          <option>Brown Gravy	</option>
                        </select>
                      </div>
                    </div>

                    <div class="col-xs-2">
                      <div class="input-group" style="width:100%">
                        <input type="text" class="form-control" placeholder="Qty">
                      </div>
                    </div>

                    <div class="col-xs-3">
                      <div class="form-group">
                        <select class="form-control" style="width: 100%;">
                          <option selected="selected">Units</option>
                          <option>KG</option>
                          <option>G</option>
                        </select>
                      </div>
                    </div>

                    <div class="col-xs-2">
                      <button type="button" class="btn btn-default btn-xs add">
                        <i class="fa fa-plus"></i>
                      </button>
                      <button type="button" class="btn btn-default btn-xs remove">
                        <i class="fa fa-minus"></i>
                      </button>
                    </div>
                  </div>


                  <div class="row repeater-row">
                    <div class="col-xs-5">
                      <div class="form-group">
                        <select class="form-control select2" style="width: 100%;">
                          <option selected="selected">Select Base</option>
                          <option>Tomato Gravy	</option>
                          <option>Red Gravy	</option>
                          <option>Brown Gravy	</option>
                        </select>
                      </div>
                    </div>

                    <div class="col-xs-2">
                      <div class="input-group" style="width:100%">
                        <input type="text" class="form-control" placeholder="Qty">
                      </div>
                    </div>

                    <div class="col-xs-3">
                      <div class="form-group">
                        <select class="form-control" style="width: 100%;">
                          <option selected="selected">Units</option>
                          <option>KG</option>
                          <option>G</option>
                        </select>
                      </div>
                    </div>

                    <div class="col-xs-2">
                      <button type="button" class="btn btn-default btn-xs add">
                        <i class="fa fa-plus"></i>
                      </button>
                      <button type="button" class="btn btn-default btn-xs remove">
                        <i class="fa fa-minus"></i>
                      </button>
                    </div>
                  </div>



                  <div class="row repeater-row">
                    <div class="col-xs-5">
                      <div class="form-group">
                        <select class="form-control select2" style="width: 100%;">
                          <option selected="selected">Select Base</option>
                          <option>Tomato Gravy	</option>
                          <option>Red Gravy	</option>
                          <option>Brown Gravy	</option>
                        </select>
                      </div>
                    </div>

                    <div class="col-xs-2">
                      <div class="input-group" style="width:100%">
                        <input type="text" class="form-control" placeholder="Qty">
                      </div>
                    </div>

                    <div class="col-xs-3">
                      <div class="form-group">
                        <select class="form-control" style="width: 100%;">
                          <option selected="selected">Units</option>
                          <option>KG</option>
                          <option>G</option>
                        </select>
                      </div>
                    </div>

                    <div class="col-xs-2">
                      <button type="button" class="btn btn-default btn-xs add">
                        <i class="fa fa-plus"></i>
                      </button>
                      <button type="button" class="btn btn-default btn-xs remove">
                        <i class="fa fa-minus"></i>
                      </button>
                    </div>
                  </div>



                  <div class="row repeater-row">
                    <div class="col-xs-5">
                      <div class="form-group">
                        <select class="form-control select2" style="width: 100%;">
                          <option selected="selected">Select Base</option>
                          <option>Tomato Gravy	</option>
                          <option>Red Gravy	</option>
                          <option>Brown Gravy	</option>
                        </select>
                      </div>
                    </div>

                    <div class="col-xs-2">
                      <div class="input-group" style="width:100%">
                        <input type="text" class="form-control" placeholder="Qty">
                      </div>
                    </div>

                    <div class="col-xs-3">
                      <div class="form-group">
                        <select class="form-control" style="width: 100%;">
                          <option selected="selected">Units</option>
                          <option>KG</option>
                          <option>G</option>
                        </select>
                      </div>
                    </div>

                    <div class="col-xs-2">
                      <button type="button" class="btn btn-default btn-xs add">
                        <i class="fa fa-plus"></i>
                      </button>
                      <button type="button" class="btn btn-default btn-xs remove">
                        <i class="fa fa-minus"></i>
                      </button>
                    </div>
                  </div>

              


                </div>
            </div>


            

              </div>

            </div>
</div>





            </div>

          </div>
        </div>
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <?php
    require_once("incld/footer_incld.php");
    require_once("incld/sidebar_incld.php");
    ?>
    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
  </div>
  <!-- ./wrapper -->

  <!-- jQuery 2.2.3 -->
  <script src="<?= base_url("assets/adminlte") ?>/plugins/jQuery/jquery-2.2.3.min.js"></script>
  <!-- Bootstrap 3.3.6 -->
  <script src="<?= base_url("assets/adminlte") ?>/bootstrap/js/bootstrap.min.js"></script>
  <!-- Select2 -->
  <script src="<?= base_url("assets/adminlte") ?>/plugins/select2/select2.full.min.js"></script>
  <!-- SlimScroll -->
  <script src="<?= base_url("assets/adminlte") ?>/plugins/slimScroll/jquery.slimscroll.min.js"></script>
  <!-- FastClick -->
  <script src="<?= base_url("assets/adminlte") ?>/plugins/fastclick/fastclick.js"></script>
  <!-- AdminLTE App -->
  <script src="<?= base_url("assets/adminlte") ?>/dist/js/app.min.js"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="<?= base_url("assets/adminlte") ?>/dist/js/demo.js"></script>
  <!-- page script -->
  <script>
    $(function() {

      $(".select2").select2({
        placeholder: "Category",
      });



    });
  </script>
</body>

</html>