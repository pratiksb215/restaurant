<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Blank Page</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?= base_url("assets/adminlte") ?>/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?= base_url("assets/adminlte") ?>/plugins/select2/select2.min.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?= base_url("assets/adminlte") ?>/plugins/datepicker/datepicker3.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url("assets/adminlte") ?>/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?= base_url("assets/adminlte") ?>/dist/css/skins/_all-skins.min.css">
  <!-- custom -->
  <link rel="stylesheet" href="<?= base_url("assets/adminlte") ?>/dist/css/custom.css">


  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>

<body class="hold-transition skin-blue sidebar-mini">
  <!-- Site wrapper -->
  <div class="wrapper">

    <?php
    require_once("incld/header_incld.php");
    require_once("incld/menu_incld.php");
    ?>

    <!-- =============================================== -->

    <!-- Modal -->
    <div class="modal fade" id="newElement" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">New Supplier</h4>
          </div>
          <div class="modal-body">
            <form>
              <div class="row">

                <div class="col-md-6">
                  <div class="form-group required">
                    <label>Name</label>
                    <input type="text" class="form-control" placeholder="Enter Name">
                    <span class="help-block">This is required</span>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group required">
                    <label>Contact Person</label>
                    <input type="text" class="form-control" placeholder="Enter Name">
                    <span class="help-block">This is required</span>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group required">
                    <label>Phone</label>
                    <input type="text" class="form-control" placeholder="Enter Name">
                    <span class="help-block">This is required</span>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group required">
                    <label>Email</label>
                    <input type="text" class="form-control" placeholder="Enter Name">
                    <span class="help-block">This is required</span>
                  </div>
                </div>

                <div class="col-md-12">
                  <div class="form-group">
                    <label>Address</label>
                    <textarea class="form-control" rows="3" placeholder="Address"></textarea>
                    <span class="help-block">This is required</span>
                  </div>
                </div>

                <div class="col-md-12">
                  <div class="form-group">
                    <label>Descriptions</label>
                    <textarea class="form-control" rows="3" placeholder="Descriptions"></textarea>
                    <span class="help-block">This is required</span>
                  </div>
                </div>


              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save</button>
          </div>
        </div>
      </div>
    </div>



    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->

      <div class="row">
        <div class="col-md-4">
          <section class="content-header">
            <h1>New Purchase</h1>
            <ol class="breadcrumb horizontal">
              <li><a href="<?= base_url("html") ?>/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
              <li><a href="<?= base_url("html") ?>/purchases">Purchases</a></li>
              <li class="active">New</li>
            </ol>
          </section>
        </div>
        <div class="col-md-8">
          <ol class="right_btn">
            <li><a href="<?= base_url("html") ?>/purchases" class="btn btn-block btn-primary"><i class="fa fa-fw fa-paper-plane-o"></i> Publish</a></li>
          </ol>
        </div>
      </div>

      <section class="content">
        <div class="row">
          <div class="col-md-12">
            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title">Purchase Details</h3>
              </div>
              <div class="box-body">
                <div class="row">
                  <div class="col-xs-3">
                    <div class="form-group required">
                      <label>Reference No</label>
                      <input type="text" class="form-control" placeholder="Reference No" disabled>
                      <span class="help-block">This is required</span>
                    </div>
                  </div>
                  <div class="col-xs-3">
                    <div class="form-group required">
                      <label>Supplier</label>
                      <select class="form-control select2" style="width: 90%;">
                        <option> Supplier 1 </option>
                        <option> Supplier 2 </option>
                      </select>
                      <i data-toggle="modal" data-target="#newElement" class="fa fa-plus"></i>
                    </div>

                  </div>

                  <div class="col-md-3">
                    <div class="form-group required">
                      <label>Date</label>
                      <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" class="form-control pull-right" id="dStart">
                      </div>
                      <!-- /.input group -->
                    </div>
                  </div>

                  <div class="col-xs-3">
                    <div class="form-group required">
                      <label>Ingredients</label>
                      <select class="form-control select2" style="width: 100%;">
                        <option> Ingredients 1 </option>
                        <option> Ingredients 2 </option>
                        <option> Ingredients 3 </option>
                        <option> Ingredients 4 </option>
                      </select>
                    </div>
                  </div>

                  <div class="col-xs-12 purchase">
                    <table>
                      <tr>
                        <th style="width:20px">#</th>
                        <th style="width:30%">Ingredient</th>
                        <th>Price ($)</th>
                        <th>Quantity</th>
                        <th>Total</th>
                        <th style="width:50px">Actions</th>
                      </tr>

                      <tr>
                        <td>1</th>
                        <td>Ingredient 1</td>
                        <td>
                          <input type="text" class="form-control" placeholder="Price">
                        </td>
                        <td>
                          <input type="text" class="form-control" placeholder="Quantity">
                        </td>
                        <td>
                          <input type="text" class="form-control" placeholder="Total" disabled>
                        </td>
                        <td>
                          <button type="button" class="btn btn-primary btn-xs">
                            <i class="fa fa-trash"></i>
                          </button>
                        </td>
                      </tr>

                      <tr>
                        <td>1</th>
                        <td>Ingredient 1</td>
                        <td>
                          <input type="text" class="form-control" placeholder="Price">
                        </td>
                        <td>
                          <input type="text" class="form-control" placeholder="Quantity">
                        </td>
                        <td>
                          <input type="text" class="form-control" placeholder="Total" disabled>
                        </td>
                        <td>
                          <button type="button" class="btn btn-primary btn-xs">
                            <i class="fa fa-trash"></i>
                          </button>
                        </td>
                      </tr>

                      <tr>
                        <td>1</th>
                        <td>Ingredient 1</td>
                        <td>
                          <input type="text" class="form-control" placeholder="Price">
                        </td>
                        <td>
                          <input type="text" class="form-control" placeholder="Quantity">
                        </td>
                        <td>
                          <input type="text" class="form-control" placeholder="Total" disabled>
                        </td>
                        <td>
                          <button type="button" class="btn btn-primary btn-xs">
                            <i class="fa fa-trash"></i>
                          </button>
                        </td>
                      </tr>

                    </table>


                    <div class="row">
                      <div class="col-md-4 pull-right">
                        <div class="form-group required">
                          <label>G. Total ($)</label>
                          <input type="text" class="form-control" disabled>
                          <span class="help-block">This is required</span>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-4 pull-right">
                        <div class="form-group required">
                          <label>Paid ($)</label>
                          <input type="text" class="form-control">
                          <span class="help-block">This is required</span>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-4 pull-right">
                        <div class="form-group required">
                          <label>Due ($)</label>
                          <input type="text" class="form-control" disabled>
                          <span class="help-block">This is required</span>
                        </div>
                      </div>
                    </div>

                  
                  </div>
                </div>
              </div>
            </div>

          </div>


        </div>







      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <?php
    require_once("incld/footer_incld.php");
    require_once("incld/sidebar_incld.php");
    ?>
    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
  </div>
  <!-- ./wrapper -->

  <!-- jQuery 2.2.3 -->
  <script src="<?= base_url("assets/adminlte") ?>/plugins/jQuery/jquery-2.2.3.min.js"></script>
  <!-- Bootstrap 3.3.6 -->
  <script src="<?= base_url("assets/adminlte") ?>/bootstrap/js/bootstrap.min.js"></script>
  <!-- Select2 -->
  <script src="<?= base_url("assets/adminlte") ?>/plugins/select2/select2.full.min.js"></script>
  <!-- SlimScroll -->
  <script src="<?= base_url("assets/adminlte") ?>/plugins/slimScroll/jquery.slimscroll.min.js"></script>
  <!-- bootstrap datepicker -->
  <script src="<?= base_url("assets/adminlte") ?>/plugins/datepicker/bootstrap-datepicker.js"></script>
  <!-- FastClick -->
  <script src="<?= base_url("assets/adminlte") ?>/plugins/fastclick/fastclick.js"></script>
  <!-- AdminLTE App -->
  <script src="<?= base_url("assets/adminlte") ?>/dist/js/app.min.js"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="<?= base_url("assets/adminlte") ?>/dist/js/demo.js"></script>
  <!-- page script -->

  <!-- page script -->
  <script src="<?= base_url("assets/adminlte") ?>/dist/js/bootstrap-multiselect.min.js"></script>
  <script>
    $(function() {

      $(".select2").select2({
        placeholder: "Category",
      });

      $('#selectType, #selectmenu, #freemenu, #selectmenu1').multiselect({
        buttonWidth: '100%',
        includeSelectAllOption: true,
        nonSelectedText: 'Select an Option'
      });

      $('#dStart, #dEnd').datepicker({
        autoclose: true
      });

    });
  </script>
</body>

</html>