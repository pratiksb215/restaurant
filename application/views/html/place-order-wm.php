<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Blank Page</title>
  <!-- Tell the browser to be responsive to screen width -->

  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?= base_url("assets/adminlte") ?>/bootstrap/css/bootstrap.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?= base_url("assets/adminlte") ?>/plugins/select2/select2.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url("assets/adminlte") ?>/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?= base_url("assets/adminlte") ?>/dist/css/skins/_all-skins.min.css">

  <link rel="stylesheet" href="<?= base_url("assets/adminlte") ?>/dist/css/custom.css">
  <link rel="stylesheet" href="<?= base_url("assets/adminlte") ?>/dist/css/jquery.scrolling-tabs.css">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<!-- ADD THE CLASS sidedar-collapse TO HIDE THE SIDEBAR PRIOR TO LOADING THE SITE -->
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->

<body class="hold-transition skin-blue layout-top-nav">
  <div class="wrapper">


    <?php
    require_once("incld/header_full_page.php");

    ?>


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Main content -->
      <section class="content">
        <div class="row">
          <div class="col-md-2">
            <div class="box running-order">
              <div class="box-header with-border">
                <h2 class="box-title">Running Orders</h2>
              </div>
              <div id="running-order">
                <div class="box box-solid order-row collapsed-box">
                  <div class="box-header ">
                   Table: 3
                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                      </button>
                    </div>
                    <!-- /.box-tools -->
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <h5> Order: B 000385</h5>
                    <h5>Waiter: John Smith </h5>
                    <h5>Customer: Sam Nick </h5>
                    <h5>Started Cooking: 2/2 </h5>
                    <h5>Done: 1/2 </h5>
                    <h5>Time Count: 20 Min</h5>
                  </div>
                  <!-- /.box-body -->
                </div>



              </div>
              <div class="box-footer">
                <button type="button" class="btn btn-default btn-block btn-sm"><i class="fa fa-edit"></i> Edit Order</button>
                <button data-toggle="modal" data-target="#orderDetails" type="button" class="btn btn-default btn-block btn-sm"><i class="fa fa-info-circle"></i> Order Details</button>
                <button data-toggle="modal" data-target="#createInvoice" type="button" class="btn btn-default btn-block btn-sm"><i class="fa fa-file-text"></i> Create Invoice</button>
                <button data-toggle="modal" data-target="#closeOrder" type="button" class="btn btn-default btn-block btn-sm"><i class="fa fa-times-circle"></i> Close Order</button>
                <button data-toggle="modal" data-target="#kitchenStatus" type="button" class="btn btn-default btn-block btn-sm"><i class="fa fa-cutlery"></i> Kitchen Status</button>
              </div>
            </div>
          </div>
          <div class="col-md-5">
            <div class="box place-order">
              <div class="row">
                <div class="col-md-12 order-btn">
                  <button data-toggle="modal" data-target="#tables" type="button" class="btn btn-default btn-md"><i class="fa fa-table"></i> Dine In</button>
                  <button type="button" class="btn btn-default btn-md"><i class="fa fa-shopping-bag"></i> Take Away</button>
                  <button type="button" class="btn btn-default btn-md"><i class="fa fa-truck"></i> Delivery</button>
                </div>
                <div class="col-md-12 order-btn">
                  <button data-toggle="modal" data-target="#holdOrderDetails" type="button" class="btn btn-default btn-mdsm"> Hold Orders</button>
                  <button data-toggle="modal" data-target="#tables" type="button" class="btn btn-default btn-md"><i class="fa fa-table"></i> Switch Table</button>
                  <button data-toggle="modal" data-target="#mergeTables" type="button" class="btn btn-default btn-md"><i class="fa fa-table"></i> Merge Table</button>
                </div>
              </div>
              <ul class="select-details">

                <li>
                  <div class="form-group required">
                    <select class="form-control select2drp" style="width: 100%;">
                      <option selected="selected">Select Waiter</option>
                      <option>John</option>
                      <option>Smith</option>
                      <option>Sam</option>
                    </select>
                    <span class="help-block">This is required</span>
                  </div>
                </li>


                <li>
                  <div class="form-group required">
                    <select class="form-control select2drp" style="width: 100%;">
                      <option selected="selected">Select Customer</option>
                      <option>John</option>
                      <option>Smith</option>
                      <option>Sam</option>
                    </select>
                    <span class="help-block">This is required</span>
                  </div>
                </li>

                <li>
                  <button data-toggle="modal" data-target="#newCustomer" type="button" class="btn btn-default btn-sm"><i class="fa fa-edit"></i></button>
                  <button data-toggle="modal" data-target="#newCustomer" type="button" class="btn btn-default btn-sm"><i class="fa fa-plus"></i> </button>
                </li>
              </ul>
              <div class="orderDetails-popup">

                <div>
                  <table class="table" style="background:#f2f2f2; margin:0">
                    <tbody>
                      <tr>
                        <th width="35%">Item</th>
                        <th width="15%">Price</th>
                        <th width="10%">Qty</th>
                        <th width="5%">Discount</th>
                        <th width="5%">Total</th>
                      </tr>
                    </tbody>
                  </table>
                  <div class="itemsList">
                    <table class="table table-hover">
                      <tbody>
                        <tr>
                          <td width="35%" class="itemAction"><i data-toggle="modal" data-target="#editItems" class="fa fa-edit"></i> <i class="fa fa-trash"></i> <abbr>Fish And Chips (021) <abbr> </td>
                          <td width="10%">$250.00</td>
                          <td width="20%" class="qtyOrder">
                            <i class="fa fa-minus"></i> 01 <i class="fa fa-plus"></i>
                          </td>
                          <td width="5%">0</td>
                          <td width="5%">$250.00</td>
                        </tr>

                        <tr>
                          <td width="35%" class="itemAction"><i data-toggle="modal" data-target="#editItems" class="fa fa-edit"></i><i class="fa fa-trash"></i> <abbr>Fish And Chips (021) <abbr> </td>
                          <td width="10%">$250.00</td>
                          <td width="20%" class="qtyOrder">
                            <i class="fa fa-minus"></i> 01 <i class="fa fa-plus"></i>
                          </td>
                          <td width="5%">0</td>
                          <td width="5%">$250.00</td>
                        </tr>


                        <tr>
                          <td width="35%" class="itemAction"><i data-toggle="modal" data-target="#editItems" class="fa fa-edit"></i> <i class="fa fa-trash"></i> <abbr>Fish And Chips (021) <abbr> </td>
                          <td width="10%">$250.00</td>
                          <td width="20%" class="qtyOrder">
                            <i class="fa fa-minus"></i> 01 <i class="fa fa-plus"></i>
                          </td>
                          <td width="5%">0</td>
                          <td width="5%">$250.00</td>
                        </tr>


                        <tr>
                          <td width="35%" class="itemAction"><i data-toggle="modal" data-target="#editItems" class="fa fa-edit"></i> <i class="fa fa-trash"></i> <abbr>Fish And Chips (021) <abbr> </td>
                          <td width="10%">$250.00</td>
                          <td width="20%" class="qtyOrder">
                            <i class="fa fa-minus"></i> 01 <i class="fa fa-plus"></i>
                          </td>
                          <td width="5%">0</td>
                          <td width="5%">$250.00</td>
                        </tr>



                        <tr>
                          <td width="35%" class="itemAction"><i data-toggle="modal" data-target="#editItems" class="fa fa-edit"></i> <i class="fa fa-trash"></i> <abbr>Fish And Chips (021) <abbr> </td>
                          <td width="10%">$250.00</td>
                          <td width="20%" class="qtyOrder">
                            <i class="fa fa-minus"></i> 01 <i class="fa fa-plus"></i>
                          </td>
                          <td width="5%">0</td>
                          <td width="5%">$250.00</td>
                        </tr>



                        <tr>
                          <td width="35%" class="itemAction"><i data-toggle="modal" data-target="#editItems" class="fa fa-edit"></i><i class="fa fa-trash"></i><abbr>Fish And Chips (021) <abbr> </td>
                          <td width="10%">$250.00</td>
                          <td width="20%" class="qtyOrder">
                            <i class="fa fa-minus"></i> 01 <i class="fa fa-plus"></i>
                          </td>
                          <td width="5%">0</td>
                          <td width="5%">$250.00</td>
                        </tr>



                        <tr>
                          <td width="35%" class="itemAction"><i data-toggle="modal" data-target="#editItems" class="fa fa-edit"></i><i class="fa fa-trash"></i> <abbr>Fish And Chips (021) <abbr> </td>
                          <td width="10%">$250.00</td>
                          <td width="20%" class="qtyOrder">
                            <i class="fa fa-minus"></i> 01 <i class="fa fa-plus"></i>
                          </td>
                          <td width="5%">0</td>
                          <td width="5%">$250.00</td>
                        </tr>



                        <tr>
                          <td width="35%" class="itemAction"><i data-toggle="modal" data-target="#editItems" class="fa fa-edit"></i> <i class="fa fa-trash"></i> <abbr>Fish And Chips (021) <abbr> </td>
                          <td width="10%">$250.00</td>
                          <td width="20%" class="qtyOrder">
                            <i class="fa fa-minus"></i> 01 <i class="fa fa-plus"></i>
                          </td>
                          <td width="5%">0</td>
                          <td width="5%">$250.00</td>
                        </tr>


                        <tr>
                          <td width="35%" class="itemAction"><i data-toggle="modal" data-target="#editItems" class="fa fa-edit"></i> <i class="fa fa-trash"></i> <abbr>Fish And Chips (021) <abbr> </td>
                          <td width="10%">$250.00</td>
                          <td width="20%" class="qtyOrder">
                            <i class="fa fa-minus"></i> 01 <i class="fa fa-plus"></i>
                          </td>
                          <td width="5%">0</td>
                          <td width="5%">$250.00</td>
                        </tr>


                      </tbody>
                    </table>
                  </div>

                </div>
                <div class="modal-footer fix-footer">
                  <div class="row">
                    <div class="col-md-4 text-left">
                      Total Item : <b>7</b>

                    </div>
                    <div class="col-md-8">
                      <ul class="order-total">
                        <li>Sub Total : <b>$543.00</b></li>
                        <li>Discount : <input type="text" class="form-control" placeholder="00"></li>
                        <li>Total Discount: <b>$00.00</b></li>
                        <li>Service/Delivery Charge : <input type="text" class="form-control" placeholder="00"></li>
                        <li>Total Payable : <b>$583.00</b></li>
                      </ul>
                    </div>
                  </div>

                </div>
              </div>
              <div class="orderbtn">
                <button data-toggle="modal" data-target="#closeOrder" type="button" class="btn btn-default btn-md"><i class="fa fa-times-circle"></i> Cancel</button>
                <button data-toggle="modal" data-target="#holdOrder" type="button" class="btn btn-default btn-md"><i class="fa fa-hand-stop-o"></i> Hold</button>
                <button data-toggle="modal" data-target="#createInvoice" type="button" class="btn btn-default btn-md"><i class="fa fa-file-text"></i> Direct Invoice</button>
                <button onclick="orderReceipt()" type="button" class="btn btn-default btn-md"><i class="fa fa-cutlery"></i> Place Order</button>
              </div>
            </div>

          </div>
          <div class="col-md-5">
            <div class="box selectmenu">
              <div class="row">


                <div class="col-md-12">
                  <div class="form-group required">

                    <input type="text" class="form-control" placeholder="Search">
                    <span class="help-block">This is required</span>
                  </div>
                </div>

                <div class="col-md-12">
                  <!-- Nav tabs -->
                  <ul class="nav nav-tabs scrollTab" role="tablist">
                    <li role="presentation" class="active"><a href="#all" role="tab" data-toggle="tab">All</a></li>
                    <li role="presentation"><a href="#meal" role="tab" data-toggle="tab">Meal</a></li>
                    <li role="presentation"><a href="#beverage" role="tab" data-toggle="tab">Beverage</a></li>

                  </ul>

                  <!-- Tab panes -->
                  <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="all">
                      <div class="selectMenuScroll">
                        <ul class="selectMenuOrder">
                          <li>
                            <abbr> $300</abbr>
                            <img src="<?= base_url("assets/adminlte") ?>/dist/img/recipes/biryani.jpg">
                            <h5>Biryani</h5>
                          </li>
                          <li>
                            <abbr> $300</abbr>
                            <img src="<?= base_url("assets/adminlte") ?>/dist/img/recipes/paneer-tikka.jpg">
                            <h5>Paneer Tikka</h5>
                          </li>

                          <li>
                            <abbr> $300</abbr>
                            <img src="<?= base_url("assets/adminlte") ?>/dist/img/recipes/biryani.jpg">
                            <h5>Biryani</h5>
                          </li>
                          <li>
                            <abbr> $300</abbr>
                            <img src="<?= base_url("assets/adminlte") ?>/dist/img/recipes/paneer-tikka.jpg">
                            <h5>Paneer Tikka</h5>
                          </li>

                          <li>
                            <abbr> $300</abbr>
                            <img src="<?= base_url("assets/adminlte") ?>/dist/img/recipes/biryani.jpg">
                            <h5>Biryani</h5>
                          </li>
                          <li>
                            <abbr> $300</abbr>
                            <img src="<?= base_url("assets/adminlte") ?>/dist/img/recipes/paneer-tikka.jpg">
                            <h5>Paneer Tikka</h5>
                          </li>

                          <li>
                            <abbr> $300</abbr>
                            <img src="<?= base_url("assets/adminlte") ?>/dist/img/recipes/biryani.jpg">
                            <h5>Biryani</h5>
                          </li>
                          <li>
                            <abbr> $300</abbr>
                            <img src="<?= base_url("assets/adminlte") ?>/dist/img/recipes/paneer-tikka.jpg">
                            <h5>Paneer Tikka</h5>
                          </li>

                          <li>
                            <abbr> $300</abbr>
                            <img src="<?= base_url("assets/adminlte") ?>/dist/img/recipes/biryani.jpg">
                            <h5>Biryani</h5>
                          </li>
                          <li>
                            <abbr> $300</abbr>
                            <img src="<?= base_url("assets/adminlte") ?>/dist/img/recipes/paneer-tikka.jpg">
                            <h5>Paneer Tikka</h5>
                          </li>






                        </ul>
                      </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="meal">
                      <div class="selectMenuScroll">
                        <ul class="selectMenuOrder">
                          <li>
                            <abbr> $300</abbr>
                            <img src="<?= base_url("assets/adminlte") ?>/dist/img/recipes/biryani.jpg">
                            <h5>Biryani</h5>
                          </li>
                          <li>
                            <abbr> $300</abbr>
                            <img src="<?= base_url("assets/adminlte") ?>/dist/img/recipes/paneer-tikka.jpg">
                            <h5>Paneer Tikka</h5>
                          </li>

                          <li>
                            <abbr> $300</abbr>
                            <img src="<?= base_url("assets/adminlte") ?>/dist/img/recipes/biryani.jpg">
                            <h5>Biryani</h5>
                          </li>
                          <li>
                            <abbr> $300</abbr>
                            <img src="<?= base_url("assets/adminlte") ?>/dist/img/recipes/paneer-tikka.jpg">
                            <h5>Paneer Tikka</h5>
                          </li>


                        </ul>
                      </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="beverage">
                      <div class="selectMenuScroll">
                        <ul class="selectMenuOrder">
                          <li>
                            <abbr> $300</abbr>
                            <img src="<?= base_url("assets/adminlte") ?>/dist/img/recipes/biryani.jpg">
                            <h5>Biryani</h5>
                          </li>




                        </ul>
                      </div>

                    </div>

                  </div>

                </div>
              </div>

            </div>
          </div>

      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <?php
    require_once("incld/footer_incld.php");
    require_once("incld/sidebar_incld.php");
    ?>
    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
  </div>


  <!-- Modal -->

  <!-- Order Details -->
  <div class="modal fade" id="orderDetails" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content orderDetails-popup">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Order Details</h4>
          <div class="orderInfo">
            <ul>
              <li><b>Order Type:</b> Dine In</li>
              <li><b>Order No.:</b> B 086489</li>
              <li><b>Waiter:</b> Smith </li>
              <li><b>Customer:</b> John</li>
              <li><b>Table:</b> 3</li>
            </ul>
          </div>
        </div>
        <div class="modal-body">
          <table class="table" style="background:#f2f2f2; margin:0">
            <tbody>
              <tr>
                <th width="52%">Item</th>
                <th width="12%">Price</th>
                <th width="12%">Qty</th>
                <th width="12%">Discount</th>
                <th width="12%">Total</th>
              </tr>

            </tbody>
          </table>
          <div class="itemsList">
            <table class="table table-hover">
              <tbody>
                <tr>
                  <td width="52%">Fish And Chips (021)</td>
                  <td width="12%">$250.00</td>
                  <td width="12%">1</td>
                  <td width="12%">0</td>
                  <td width="12%">$250.00</td>
                </tr>
                <tr>
                  <td width="52%">Fish And Chips (021)</td>
                  <td width="12%">$250.00</td>
                  <td width="12%">1</td>
                  <td width="12%">0</td>
                  <td width="12%">$250.00</td>
                </tr>
                <tr>
                  <td width="52%">Fish And Chips (021)</td>
                  <td width="12%">$250.00</td>
                  <td width="12%">1</td>
                  <td width="12%">0</td>
                  <td width="12%">$250.00</td>
                </tr>
                <tr>
                  <td width="52%">Fish And Chips (021)</td>
                  <td width="12%">$250.00</td>
                  <td width="12%">1</td>
                  <td width="12%">0</td>
                  <td width="12%">$250.00</td>
                </tr>
                <tr>
                  <td width="52%">Fish And Chips (021)</td>
                  <td width="12%">$250.00</td>
                  <td width="12%">1</td>
                  <td width="12%">0</td>
                  <td width="12%">$250.00</td>
                </tr>
                <tr>
                  <td width="52%">Fish And Chips (021)</td>
                  <td width="12%">$250.00</td>
                  <td width="12%">1</td>
                  <td width="12%">0</td>
                  <td width="12%">$250.00</td>
                </tr>

              </tbody>
            </table>
          </div>

        </div>
        <div class="modal-footer fix-footer">
          <div class="row">
            <div class="col-md-4 text-left">
              Total Item : <b>7</b>

            </div>
            <div class="col-md-8">
              <ul>
                <li>Sub Total : <b>$543.00</b></li>
                <li>Discount : <b>$00.00</b></li>
                <li>VAT : <b>$00.00</b></li>
                <li>SGST : <b>$10.00</b></li>
                <li>CGST : <b>$10.00</b></li>
                <li>Total Discount: <b>$00.00</b></li>
                <li>Service/Delivery Charge : <b>$20.00</b></li>
                <li>Total Payable : <b>$583.00</b></li>
              </ul>
            </div>
          </div>
          <div>
            <button data-toggle="modal" data-target="#createInvoice" type="button" class="btn btn-info btn-sm"><i class="fa fa-file-text"></i> Create Invoice</button>
            <button type="button" class="btn btn-danger btn-sm"><i class="fa fa-times-circle"></i> Close Order</button>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Hold Order  -->
  <div class="modal fade" id="holdOrderDetails" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="width:1000px">
      <div class="modal-content orderDetails-popup">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Hold Order Details</h4>

        </div>


        <div class="modal-body">
          <div class="row">
            <div class="col-md-4">
              <table class="table">
                <tr>
                  <th width="150px">Hold Number</th>
                  <th>Customer</th>

                </tr>
                <tr>
                  <td>1</td>
                  <td>Smith</td>

                </tr>
                <tr>
                  <td>2</td>
                  <td>Smith</td>

                </tr>
              </table>
              <button type="button" class="btn btn-sm"></i>Delete All Hold Orders</button>
            </div>

            <div class="col-md-8">

              <table class="table" style="background:#f2f2f2; margin:0">
                <tbody>
                  <tr>
                    <th width="52%">Item</th>
                    <th width="12%">Price</th>
                    <th width="12%">Qty</th>
                    <th width="12%">Discount</th>
                    <th width="12%">Total</th>
                  </tr>

                </tbody>
              </table>
              <div class="itemsList">
                <table class="table table-hover">
                  <tbody>
                    <tr>
                      <td width="52%">Fish And Chips (021)</td>
                      <td width="12%">$250.00</td>
                      <td width="12%">1</td>
                      <td width="12%">0</td>
                      <td width="12%">$250.00</td>
                    </tr>
                    <tr>
                      <td width="52%">Fish And Chips (021)</td>
                      <td width="12%">$250.00</td>
                      <td width="12%">1</td>
                      <td width="12%">0</td>
                      <td width="12%">$250.00</td>
                    </tr>
                    <tr>
                      <td width="52%">Fish And Chips (021)</td>
                      <td width="12%">$250.00</td>
                      <td width="12%">1</td>
                      <td width="12%">0</td>
                      <td width="12%">$250.00</td>
                    </tr>
                    <tr>
                      <td width="52%">Fish And Chips (021)</td>
                      <td width="12%">$250.00</td>
                      <td width="12%">1</td>
                      <td width="12%">0</td>
                      <td width="12%">$250.00</td>
                    </tr>
                    <tr>
                      <td width="52%">Fish And Chips (021)</td>
                      <td width="12%">$250.00</td>
                      <td width="12%">1</td>
                      <td width="12%">0</td>
                      <td width="12%">$250.00</td>
                    </tr>
                    <tr>
                      <td width="52%">Fish And Chips (021)</td>
                      <td width="12%">$250.00</td>
                      <td width="12%">1</td>
                      <td width="12%">0</td>
                      <td width="12%">$250.00</td>
                    </tr>

                  </tbody>
                </table>
              </div>


              <div class="fix-footer">
                <div class="row ">
                  <div class="col-md-4 text-left">
                    Total Item : <b>7</b>

                  </div>
                  <div class="col-md-8">
                    <ul>
                      <li>Sub Total : <b>$543.00</b></li>
                      <li>Discount : <b>$00.00</b></li>
                      <li>VAT : <b>$00.00</b></li>
                      <li>SGST : <b>$10.00</b></li>
                      <li>CGST : <b>$10.00</b></li>
                      <li>Total Discount: <b>$00.00</b></li>
                      <li>Service/Delivery Charge : <b>$20.00</b></li>
                      <li>Total Payable : <b>$583.00</b></li>
                    </ul>
                  </div>
                </div>
              </div>

            </div>
          </div>





        </div>
        <div class="modal-footer">

          <div>
            <button type="button" class="btn btn-info btn-sm"><i class="fa fa-file-text"></i> Edit in cart</button>
            <button type="button" class="btn btn-danger btn-sm"><i class="fa fa-times-circle"></i> Cancel</button>
            <button type="button" class="btn btn-danger btn-sm"><i class="fa fa-times-circle"></i> Delete</button>
          </div>
        </div>
      </div>
    </div>
  </div>


  <!-- Invoice Details -->
  <div class="modal fade" id="createInvoice" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog " role="document" style="width:400px">
      <div class="modal-content createInvoice">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Create Invoice</h4>

        </div>
        <div class="modal-body ">
          <div class="row">
            <div class="col-md-6 ">
              Total Payable:
              <h3>$345:00</h3>
            </div>
            <div class="col-md-6">
              <div class="form-group required">
                <label>Payment Method</label>
                <select class="form-control select2" style="width: 100%;">
                  <option selected="selected">Cash</option>
                  <option>Debit Card</option>
                  <option>Credit Card</option>
                </select>
                <span class="help-block">This is required</span>
              </div>
            </div>

            <div class="col-md-6">
              <div class="form-group required">
                <label>Pay Amount</label>
                <input type="text" class="form-control" placeholder="Enter Name">
                <span class="help-block">This is required</span>
              </div>
            </div>

            <div class="col-md-6">
              <div class="form-group required">
                <label>Due Amount</label>
                <input type="text" class="form-control" value="00" disabled>
                <span class="help-block">This is required</span>
              </div>
            </div>

            <div class="col-md-6">
              <div class="form-group required">
                <label>Given Amount</label>
                <input type="text" class="form-control" placeholder="Enter Name">
                <span class="help-block">This is required</span>
              </div>
            </div>

            <div class="col-md-6">
              <div class="form-group required">
                <label>Change Amount</label>
                <input type="text" class="form-control" value="00" disabled>
                <span class="help-block">This is required</span>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" onclick="invoicePrint()" >Create Invoice</button>
        </div>
      </div>
    </div>
  </div>

  <!-- Close Order -->
  <div class="modal fade" id="closeOrder" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog " role="document" style="width:400px">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Close Order</h4>

        </div>
        <div class="modal-body text-center ">
          <h4> Please select order to close. </h4>
        </div>
        <div class="modal-footer text-center">
          <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>

        </div>
      </div>
    </div>
  </div>

  <!-- Hold Order -->
  <div class="modal fade" id="holdOrder" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog " role="document" style="width:400px">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Hold Order</h4>

        </div>
        <div class="modal-body ">

          <div class="form-group required">
            <label>Hold Number</label>
            <input type="text" class="form-control" placeholder="00">
            <span class="help-block">This is required</span>
          </div>

        </div>
        <div class="modal-footer ">

          <button type="button" class="btn" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-primary" data-dismiss="modal">Save</button>
        </div>
      </div>
    </div>
  </div>

  <!-- kitchen Status -->
  <div class="modal fade" id="kitchenStatus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content orderDetails-popup">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Kitchen Status</h4>
          <div class="orderInfo">
            <ul>
              <li><b>Order Type:</b> Dine In</li>
              <li><b>Order No.:</b> B 086489</li>
              <li><b>Waiter:</b> Smith </li>
              <li><b>Customer:</b> John</li>
              <li><b>Table:</b> 3</li>
            </ul>
          </div>
        </div>
        <div class="modal-body">
          <table class="table" style="background:#f2f2f2; margin:0">
            <tbody>
              <tr>
                <th width="70%">Item</th>
                <th width="15%">Qty</th>
                <th width="15%">Status</th>
              </tr>

            </tbody>
          </table>
          <div class="itemsList">
            <table class="table table-hover">
              <tbody>
                <tr>
                  <td width="70%">Fish And Chips (021)</td>
                  <td width="15%">1</td>
                  <td width="15%"><span class="label label-success">Completed</span></td>
                </tr>
                <tr>
                  <td width="70%">Fish And Chips (021)</td>
                  <td width="15%">1</td>
                  <td width="15%"><span class="label label-info">In the queue</span></td>
                </tr>
                <tr>
                  <td width="70%">Fish And Chips (021)</td>
                  <td width="15%">1</td>
                  <td width="15%"><span class="label label-warning">Hold</span></td>
                </tr>
                <tr>
                  <td width="70%">Fish And Chips (021)</td>
                  <td width="15%">1</td>
                  <td width="15%"><span class="label label-danger">Cancelled</span></td>
                </tr>


              </tbody>
            </table>
          </div>

        </div>
        <div class="modal-footer">
          <div class="row">
            <div class="col-md-4 text-left">
              Order Placed at : <b> 3:56</b>

            </div>
            <div class="col-md-4 text-left">
              Time Count: <b>62:10 M</b>
            </div>
            <div class="col-md-4 ">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>

  <!-- New Customer Status -->
  <div class="modal fade" id="newCustomer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">New Customer</h4>
        </div>
        <div class="modal-body">
          <form>
            <div class="row">

              <div class="col-md-12">
                <div class="form-group required">
                  <label>Name</label>
                  <input type="text" class="form-control" placeholder="Enter Name">
                  <span class="help-block">This is required</span>
                </div>
              </div>



              <div class="col-md-6">
                <div class="form-group required">
                  <label>Phone</label>
                  <input type="text" class="form-control" placeholder="Enter Name">
                  <span class="help-block">This is required</span>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group required">
                  <label>Email</label>
                  <input type="text" class="form-control" placeholder="Enter Name">
                  <span class="help-block">This is required</span>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group required">
                  <label>Date of birth</label>

                  <div class="input-group date">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control pull-right" id="dob">
                  </div>
                  <!-- /.input group -->
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group required">
                  <label>Date of anniversary</label>
                  <div class="input-group date">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control pull-right" id="doa">
                  </div>
                  <!-- /.input group -->
                </div>
              </div>

              <div class="col-md-12">
                <div class="form-group">
                  <label>Address</label>
                  <textarea class="form-control" rows="3" placeholder="Address"></textarea>
                  <span class="help-block">This is required</span>
                </div>
              </div>




            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Save</button>
        </div>
      </div>
    </div>
  </div>

  <!-- edit Item  -->
  <div class="modal fade" id="editItems" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="width:400px">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Fish And Chips (021)</h4>
        </div>
        <div class="modal-body ">
          <form>
            <div class="row itemsedit">
              <div class="col-md-3">
                <h4> Quantity </h4>
              </div>
              <div class="col-md-5">
                <button type="button" class="btn btn-default btn-sm"><i class="fa fa-minus"></i> </button>
                <input type="text" style="width: 50px; display: inline-block; vertical-align:middle " class="form-control" placeholder="1">
                <button type="button" class="btn btn-default btn-sm"><i class="fa fa-plus"></i> </button>
              </div>
              <div class="col-md-2">
                <h4> $234 </h4>
              </div>





              <div class="col-md-12">
                <div class="form-group required">
                  <label>Discount</label>
                  <input type="text" class="form-control" placeholder="00">
                  <span class="help-block">This is required</span>
                </div>
              </div>


              <div class="col-md-12">
                <div class="form-group">
                  <label>Note</label>
                  <textarea class="form-control" rows="3" placeholder="Note"></textarea>
                  <span class="help-block">This is required</span>
                </div>
              </div>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Save</button>
        </div>
      </div>
    </div>
  </div>

  <!-- tables  -->
  <div class="modal fade" id="tables" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Select Table</h4>
        </div>
        <div class="modal-body">
          <form>
            <div class="row">


              <div class="col-md-12">
                <div class="form-group">
                  <div id="mytable">
                    <div class="radio select-table">
                      <label>
                        <abbr><img src="<?= base_url("assets/adminlte") ?>/dist/img/table-icon-active.png" class="user-image"> <abbr>

                            <span>Table No. 1</span>
                            <input type="radio" name="optionsRadios" id="table1" value="table1" disabled>
                      </label>
                      <label>
                        <abbr><img src="<?= base_url("assets/adminlte") ?>/dist/img/table-icon-active.png" class="user-image"> <abbr>

                            <span>Table No. 2</span>
                            <input type="radio" name="optionsRadios" id="table2" value="table2" disabled>
                      </label>

                      <label>
                        <abbr><img src="<?= base_url("assets/adminlte") ?>/dist/img/table-icon.png" checked="" class="user-image"> <abbr>

                            <span>Table No. 3</span>
                            <input type="radio" name="optionsRadios" id="table3" value="table3">
                      </label>

                      <label>
                        <abbr><img src="<?= base_url("assets/adminlte") ?>/dist/img/table-icon.png" class="user-image"> <abbr>

                            <span>Table No. 4</span>
                            <input type="radio" name="optionsRadios" id="table4" value="table4">
                      </label>

                      <label>
                        <abbr><img src="<?= base_url("assets/adminlte") ?>/dist/img/table-icon.png" class="user-image"> <abbr>

                            <span>Table No. 5</span>
                            <input type="radio" name="optionsRadios" id="table5" value="table5">
                      </label>

                      <label>
                        <abbr><img src="<?= base_url("assets/adminlte") ?>/dist/img/table-icon.png" class="user-image"> <abbr>

                            <span>Table No. 6</span>
                            <input type="radio" name="optionsRadios" id="table6" value="table6">
                      </label>



                    </div>

                  </div>

                  <div id="switchtable">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group required">
                          <select class="form-control select2drp" style="width: 100%;">
                            <option selected="selected">Select Waiter</option>
                            <option>John</option>
                            <option>Smith</option>
                            <option>Sam</option>
                          </select>
                          <span class="help-block">This is required</span>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group required">

                          <input type="text" class="form-control" placeholder="Search Table">
                          <span class="help-block">This is required</span>
                        </div>
                      </div>
                    </div>
                    <div class="tableScroll">
                      <div class="radio select-table">
                        <label>
                          <abbr><img src="<?= base_url("assets/adminlte") ?>/dist/img/table-icon.png" class="user-image"> <abbr>

                              <span>Table No. 7</span>
                              <input type="radio" name="optionsRadios" id="table1" value="table1">
                        </label>
                        <label>
                          <abbr><img src="<?= base_url("assets/adminlte") ?>/dist/img/table-icon.png" class="user-image"> <abbr>

                              <span>Table No. 8</span>
                              <input type="radio" name="optionsRadios" id="table2" value="table2">
                        </label>

                        <label>
                          <abbr><img src="<?= base_url("assets/adminlte") ?>/dist/img/table-icon.png" checked="" class="user-image"> <abbr>

                              <span>Table No. 9</span>
                              <input type="radio" name="optionsRadios" id="table3" value="table3">
                        </label>

                        <label>
                          <abbr><img src="<?= base_url("assets/adminlte") ?>/dist/img/table-icon.png" class="user-image"> <abbr>

                              <span>Table No. 10</span>
                              <input type="radio" name="optionsRadios" id="table4" value="table4">
                        </label>

                        <label>
                          <abbr><img src="<?= base_url("assets/adminlte") ?>/dist/img/table-icon.png" class="user-image"> <abbr>

                              <span>Table No. 10</span>
                              <input type="radio" name="optionsRadios" id="table5" value="table5">
                        </label>

                        <label>
                          <abbr><img src="<?= base_url("assets/adminlte") ?>/dist/img/table-icon.png" class="user-image"> <abbr>

                              <span>Table No. 12</span>
                              <input type="radio" name="optionsRadios" id="table6" value="table6">
                        </label>

                        <label>
                          <abbr><img src="<?= base_url("assets/adminlte") ?>/dist/img/table-icon.png" class="user-image"> <abbr>

                              <span>Table No. 7</span>
                              <input type="radio" name="optionsRadios" id="table1" value="table1">
                        </label>
                        <label>
                          <abbr><img src="<?= base_url("assets/adminlte") ?>/dist/img/table-icon.png" class="user-image"> <abbr>

                              <span>Table No. 8</span>
                              <input type="radio" name="optionsRadios" id="table2" value="table2">
                        </label>

                        <label>
                          <abbr><img src="<?= base_url("assets/adminlte") ?>/dist/img/table-icon.png" checked="" class="user-image"> <abbr>

                              <span>Table No. 9</span>
                              <input type="radio" name="optionsRadios" id="table3" value="table3">
                        </label>

                        <label>
                          <abbr><img src="<?= base_url("assets/adminlte") ?>/dist/img/table-icon.png" class="user-image"> <abbr>

                              <span>Table No. 10</span>
                              <input type="radio" name="optionsRadios" id="table4" value="table4">
                        </label>

                        <label>
                          <abbr><img src="<?= base_url("assets/adminlte") ?>/dist/img/table-icon.png" class="user-image"> <abbr>

                              <span>Table No. 10</span>
                              <input type="radio" name="optionsRadios" id="table5" value="table5">
                        </label>

                        <label>
                          <abbr><img src="<?= base_url("assets/adminlte") ?>/dist/img/table-icon.png" class="user-image"> <abbr>

                              <span>Table No. 12</span>
                              <input type="radio" name="optionsRadios" id="table6" value="table6">
                        </label>

                        <label>
                          <abbr><img src="<?= base_url("assets/adminlte") ?>/dist/img/table-icon.png" class="user-image"> <abbr>

                              <span>Table No. 7</span>
                              <input type="radio" name="optionsRadios" id="table1" value="table1">
                        </label>
                        <label>
                          <abbr><img src="<?= base_url("assets/adminlte") ?>/dist/img/table-icon.png" class="user-image"> <abbr>

                              <span>Table No. 8</span>
                              <input type="radio" name="optionsRadios" id="table2" value="table2">
                        </label>

                        <label>
                          <abbr><img src="<?= base_url("assets/adminlte") ?>/dist/img/table-icon.png" checked="" class="user-image"> <abbr>

                              <span>Table No. 9</span>
                              <input type="radio" name="optionsRadios" id="table3" value="table3">
                        </label>

                        <label>
                          <abbr><img src="<?= base_url("assets/adminlte") ?>/dist/img/table-icon.png" class="user-image"> <abbr>

                              <span>Table No. 10</span>
                              <input type="radio" name="optionsRadios" id="table4" value="table4">
                        </label>

                        <label>
                          <abbr><img src="<?= base_url("assets/adminlte") ?>/dist/img/table-icon.png" class="user-image"> <abbr>

                              <span>Table No. 10</span>
                              <input type="radio" name="optionsRadios" id="table5" value="table5">
                        </label>

                        <label>
                          <abbr><img src="<?= base_url("assets/adminlte") ?>/dist/img/table-icon.png" class="user-image"> <abbr>

                              <span>Table No. 12</span>
                              <input type="radio" name="optionsRadios" id="table6" value="table6">
                        </label>




                      </div>
                    </div>
                  </div>




                </div>
              </div>




            </div>
          </form>
        </div>
        <div class="modal-footer">
          <div class="row">
            <div class="col-md-6 text-left">
              <button type="button" id="switchTableBtn"  class="btn btn-danger">Switch Table</button>
              <button type="button" id="myTableBtn"  class="btn btn-danger">My Tables</button>
            </div>
            <div class="col-md-6">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary">Save</button>

            </div>
          </div>




        </div>
      </div>
    </div>
  </div>

    <!-- Merge tables  -->
    <div class="modal fade" id="mergeTables" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Merge Table</h4>
        </div>
        <div class="modal-body">
          <form>
            <div class="row">


              <div class="col-md-12">
                <div class="form-group">
               

                  <div id="switchtable">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group required">
                          <select class="form-control select2drp" style="width: 100%;">
                            <option selected="selected">Select Waiter</option>
                            <option>John</option>
                            <option>Smith</option>
                            <option>Sam</option>
                          </select>
                          <span class="help-block">This is required</span>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group required">

                          <input type="text" class="form-control" placeholder="Search Table">
                          <span class="help-block">This is required</span>
                        </div>
                      </div>
                    </div>
                    <div class="tableScroll">
                      <div class="checkbox select-table">
                        <label>
                          <abbr><img src="<?= base_url("assets/adminlte") ?>/dist/img/table-icon.png" class="user-image"> <abbr>

                              <span>Table No. 7</span>
                              <input type="checkbox" name="optionsRadios" id="table1" value="table1">
                        </label>
                        <label>
                          <abbr><img src="<?= base_url("assets/adminlte") ?>/dist/img/table-icon.png" class="user-image"> <abbr>

                              <span>Table No. 8</span>
                              <input type="checkbox" name="optionsRadios" id="table2" value="table2">
                        </label>

                        <label>
                          <abbr><img src="<?= base_url("assets/adminlte") ?>/dist/img/table-icon.png" checked="" class="user-image"> <abbr>

                              <span>Table No. 9</span>
                              <input type="checkbox" name="optionsRadios" id="table3" value="table3">
                        </label>

                        <label>
                          <abbr><img src="<?= base_url("assets/adminlte") ?>/dist/img/table-icon.png" class="user-image"> <abbr>

                              <span>Table No. 10</span>
                              <input type="checkbox" name="optionsRadios" id="table4" value="table4">
                        </label>

                        <label>
                          <abbr><img src="<?= base_url("assets/adminlte") ?>/dist/img/table-icon.png" class="user-image"> <abbr>

                              <span>Table No. 10</span>
                              <input type="checkbox" name="optionsRadios" id="table5" value="table5">
                        </label>

                        <label>
                          <abbr><img src="<?= base_url("assets/adminlte") ?>/dist/img/table-icon.png" class="user-image"> <abbr>

                              <span>Table No. 12</span>
                              <input type="checkbox" name="optionsRadios" id="table6" value="table6">
                        </label>

                        <label>
                          <abbr><img src="<?= base_url("assets/adminlte") ?>/dist/img/table-icon.png" class="user-image"> <abbr>

                              <span>Table No. 7</span>
                              <input type="checkbox" name="optionsRadios" id="table1" value="table1">
                        </label>
                        <label>
                          <abbr><img src="<?= base_url("assets/adminlte") ?>/dist/img/table-icon.png" class="user-image"> <abbr>

                              <span>Table No. 8</span>
                              <input type="checkbox" name="optionsRadios" id="table2" value="table2">
                        </label>

                        <label>
                          <abbr><img src="<?= base_url("assets/adminlte") ?>/dist/img/table-icon.png" checked="" class="user-image"> <abbr>

                              <span>Table No. 9</span>
                              <input type="checkbox" name="optionsRadios" id="table3" value="table3">
                        </label>

                        <label>
                          <abbr><img src="<?= base_url("assets/adminlte") ?>/dist/img/table-icon.png" class="user-image"> <abbr>

                              <span>Table No. 10</span>
                              <input type="checkbox" name="optionsRadios" id="table4" value="table4">
                        </label>

                        <label>
                          <abbr><img src="<?= base_url("assets/adminlte") ?>/dist/img/table-icon.png" class="user-image"> <abbr>

                              <span>Table No. 10</span>
                              <input type="checkbox" name="optionsRadios" id="table5" value="table5">
                        </label>

                        <label>
                          <abbr><img src="<?= base_url("assets/adminlte") ?>/dist/img/table-icon.png" class="user-image"> <abbr>

                              <span>Table No. 12</span>
                              <input type="checkbox" name="optionsRadios" id="table6" value="table6">
                        </label>

                        <label>
                          <abbr><img src="<?= base_url("assets/adminlte") ?>/dist/img/table-icon.png" class="user-image"> <abbr>

                              <span>Table No. 7</span>
                              <input type="checkbox" name="optionsRadios" id="table1" value="table1">
                        </label>
                        <label>
                          <abbr><img src="<?= base_url("assets/adminlte") ?>/dist/img/table-icon.png" class="user-image"> <abbr>

                              <span>Table No. 8</span>
                              <input type="checkbox" name="optionsRadios" id="table2" value="table2">
                        </label>

                        <label>
                          <abbr><img src="<?= base_url("assets/adminlte") ?>/dist/img/table-icon.png" checked="" class="user-image"> <abbr>

                              <span>Table No. 9</span>
                              <input type="checkbox" name="optionsRadios" id="table3" value="table3">
                        </label>

                        <label>
                          <abbr><img src="<?= base_url("assets/adminlte") ?>/dist/img/table-icon.png" class="user-image"> <abbr>

                              <span>Table No. 10</span>
                              <input type="checkbox" name="optionsRadios" id="table4" value="table4">
                        </label>

                        <label>
                          <abbr><img src="<?= base_url("assets/adminlte") ?>/dist/img/table-icon.png" class="user-image"> <abbr>

                              <span>Table No. 10</span>
                              <input type="checkbox" name="optionsRadios" id="table5" value="table5">
                        </label>

                        <label>
                          <abbr><img src="<?= base_url("assets/adminlte") ?>/dist/img/table-icon.png" class="user-image"> <abbr>

                              <span>Table No. 12</span>
                              <input type="checkbox" name="optionsRadios" id="table6" value="table6">
                        </label>




                      </div>
                    </div>
                  </div>




                </div>
              </div>




            </div>
          </form>
        </div>
        <div class="modal-footer">

          

              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary">Save</button>


        </div>
      </div>
    </div>
  </div>

  <!-- ./wrapper -->

  <!-- jQuery 2.2.3 -->
  <script src="<?= base_url("assets/adminlte") ?>/plugins/jQuery/jquery-2.2.3.min.js"></script>
  <!-- Bootstrap 3.3.6 -->
  <script src="<?= base_url("assets/adminlte") ?>/bootstrap/js/bootstrap.min.js"></script>
  <!-- Select2 -->
  <script src="<?= base_url("assets/adminlte") ?>/plugins/select2/select2.full.min.js"></script>
  <!-- SlimScroll -->
  <script src="<?= base_url("assets/adminlte") ?>/plugins/slimScroll/jquery.slimscroll.min.js"></script>
  <!-- FastClick -->
  <script src="<?= base_url("assets/adminlte") ?>/plugins/fastclick/fastclick.js"></script>
  <!-- AdminLTE App -->
  <script src="<?= base_url("assets/adminlte") ?>/dist/js/app.min.js"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="<?= base_url("assets/adminlte") ?>/dist/js/demo.js"></script>
  <script src="<?= base_url("assets/adminlte") ?>/dist/js/jquery.scrolling-tabs.js"></script>


  <script>
    $(function() {

      mytable =  $('#mytable');
      switchtable =  $('#switchtable');
      myTableBtn =  $('#myTableBtn');  
      switchBtn =  $('#switchTableBtn');  

      switchtable.hide();
      myTableBtn.hide();

      switchBtn.click(function(){
      
        $(this).hide();
        mytable.hide();
        switchtable.show();
        myTableBtn.show();
      })

      myTableBtn.click(function(){
      
        $(this).hide();
        switchtable.hide();
        switchBtn.show();
        mytable.show();
      })




      $(".select2").select2({
        dropdownParent: $('#createInvoice')
      });

      $(".select2drp").select2({

      });

      $('#running-order').slimScroll({
        height: '300px'
      });

      $('.itemsList').slimScroll({
        height: '150px'
      });

      $('.selectMenuScroll').slimScroll({
        height: '400px'
      });

      $('.tableScroll').slimScroll({
        height: '300px'
      });




      $('#running-order .box .box-tools').each(function() {
        $(this).click(function() {
          getEle = $(this).parents('.order-row').siblings();
          getEle.addClass('collapsed-box');
          getEle.find('.box-body').hide()
          getEle.find('.box-tools  i').removeClass("fa-minus").addClass("fa-plus")
        });
      });

      $('.order-type button').each(function() {
        $(this).click(function() {
          getEle = $(this).siblings();
          $(this).removeClass("btn-default").addClass('btn-primary');
          getEle.removeClass("btn-primary").addClass('btn-default')
        });
      });


      $('.scrollTab')
        .scrollingTabs()
        .on('ready.scrtabs', function() {
          $('.tab-content').show();
        });



    });


function invoicePrint() {
  var myWindow = window.open("http://localhost/ci/html/invoice", "", "width=600,height=600");
}

function orderReceipt() {
  var myWindow = window.open("http://localhost/ci/html/order_receipt", "", "width=600,height=600");
}
  </script>

</body>

</html>