<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Blank Page</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?= base_url("assets/adminlte") ?>/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">

  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url("assets/adminlte") ?>/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?= base_url("assets/adminlte") ?>/dist/css/skins/_all-skins.min.css">
  <!-- custom -->
  <link rel="stylesheet" href="<?= base_url("assets/adminlte") ?>/dist/css/custom.css">


  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>

<body class="hold-transition skin-blue sidebar-mini">
  <!-- Site wrapper -->
  <div class="wrapper">

    <?php
    require_once("incld/header_incld.php");
    require_once("incld/menu_incld.php");
    ?>

    <!-- =============================================== -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->

      <div class="row">
        <div class="col-md-12">
          <section class="content-header">
            <h1>Settings</h1>
            <ol class="breadcrumb horizontal">
              <li><a href="<?= base_url("html") ?>/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
              <li class="active">Settings</li>
            </ol>
          </section>
        </div>

      </div>




      <!-- Main content -->
      <section class="content">

        <div class="nav-tabs-custom">
          <ul class="nav nav-tabs">
            <li class="active"><a href="#General" data-toggle="tab" aria-expanded="false">General</a></li>
            <li class=""><a href="#Email" data-toggle="tab" aria-expanded="false">Email</a></li>
            <li class=""><a href="#SMS" data-toggle="tab" aria-expanded="false">SMS</a></li>
            <li class=""><a href="#Tax" data-toggle="tab" aria-expanded="true">Tax</a></li>
          </ul>
          <div class="tab-content tab-padding">
            <div class="tab-pane active" id="General">
              <div class="row">
                <div class="col-md-6">

                  <div class="form-group required">
                    <label>Date Format </label>
                    <select class="form-control select2" style="width: 100%;">
                      <option>DD/MM/YYYY</option>
                      <option>MM/DD/YYYY</option>
                    </select>
                    <span class="help-block">This is required</span>
                  </div>

                  <div class="form-group required">
                    <label>Country Time Zone</label>
                    <select class="form-control select2" style="width: 100%;">
                      <option value="-12:00">(GMT -12:00) Eniwetok, Kwajalein</option>
                      <option value="-11:00">(GMT -11:00) Midway Island, Samoa</option>
                      <option value="-10:00">(GMT -10:00) Hawaii</option>
                      <option value="-09:50">(GMT -9:30) Taiohae</option>
                      <option value="-09:00">(GMT -9:00) Alaska</option>
                      <option value="-08:00">(GMT -8:00) Pacific Time (US &amp; Canada)</option>
                      <option value="-07:00">(GMT -7:00) Mountain Time (US &amp; Canada)</option>
                      <option value="-06:00">(GMT -6:00) Central Time (US &amp; Canada), Mexico City</option>
                      <option value="-05:00">(GMT -5:00) Eastern Time (US &amp; Canada), Bogota, Lima</option>
                      <option value="-04:50">(GMT -4:30) Caracas</option>
                      <option value="-04:00">(GMT -4:00) Atlantic Time (Canada), Caracas, La Paz</option>
                      <option value="-03:50">(GMT -3:30) Newfoundland</option>
                      <option value="-03:00">(GMT -3:00) Brazil, Buenos Aires, Georgetown</option>
                      <option value="-02:00">(GMT -2:00) Mid-Atlantic</option>
                      <option value="-01:00">(GMT -1:00) Azores, Cape Verde Islands</option>
                      <option value="+00:00" selected="selected">(GMT) Western Europe Time, London, Lisbon, Casablanca</option>
                      <option value="+01:00">(GMT +1:00) Brussels, Copenhagen, Madrid, Paris</option>
                      <option value="+02:00">(GMT +2:00) Kaliningrad, South Africa</option>
                      <option value="+03:00">(GMT +3:00) Baghdad, Riyadh, Moscow, St. Petersburg</option>
                      <option value="+03:50">(GMT +3:30) Tehran</option>
                      <option value="+04:00">(GMT +4:00) Abu Dhabi, Muscat, Baku, Tbilisi</option>
                      <option value="+04:50">(GMT +4:30) Kabul</option>
                      <option value="+05:00">(GMT +5:00) Ekaterinburg, Islamabad, Karachi, Tashkent</option>
                      <option value="+05:50">(GMT +5:30) Bombay, Calcutta, Madras, New Delhi</option>
                      <option value="+05:75">(GMT +5:45) Kathmandu, Pokhara</option>
                      <option value="+06:00">(GMT +6:00) Almaty, Dhaka, Colombo</option>
                      <option value="+06:50">(GMT +6:30) Yangon, Mandalay</option>
                      <option value="+07:00">(GMT +7:00) Bangkok, Hanoi, Jakarta</option>
                      <option value="+08:00">(GMT +8:00) Beijing, Perth, Singapore, Hong Kong</option>
                      <option value="+08:75">(GMT +8:45) Eucla</option>
                      <option value="+09:00">(GMT +9:00) Tokyo, Seoul, Osaka, Sapporo, Yakutsk</option>
                      <option value="+09:50">(GMT +9:30) Adelaide, Darwin</option>
                      <option value="+10:00">(GMT +10:00) Eastern Australia, Guam, Vladivostok</option>
                      <option value="+10:50">(GMT +10:30) Lord Howe Island</option>
                      <option value="+11:00">(GMT +11:00) Magadan, Solomon Islands, New Caledonia</option>
                      <option value="+11:50">(GMT +11:30) Norfolk Island</option>
                      <option value="+12:00">(GMT +12:00) Auckland, Wellington, Fiji, Kamchatka</option>
                      <option value="+12:75">(GMT +12:45) Chatham Islands</option>
                      <option value="+13:00">(GMT +13:00) Apia, Nukualofa</option>
                      <option value="+14:00">(GMT +14:00) Line Islands, Tokelau</option>
                    </select>
                    <span class="help-block">This is required</span>
                  </div>

                  <div class="form-group required">
                    <label>Currency </label>
                    <select class="form-control select2" style="width: 100%;">
                      <option>Rupee</option>
                      <option>US Doller</option>
                      <option>Euro</option>
                    </select>
                    <span class="help-block">This is required</span>
                  </div>

                  <div class="form-group">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <button type="submit" class="btn btn-danger">Reset</button>
                  </div>


                </div>
              </div>
            </div>
            <div class="tab-pane" id="Email">
              <div class="row">
                <div class="col-md-6">
                 
                  <div class="form-group">
                    <label for="exampleInputEmail1">From Email</label>
                    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter...">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">From Name</label>
                    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter...">
                  </div>
                  <div class="form-group">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <button type="submit" class="btn btn-danger">Reset</button>
                  </div>

                </div>
              </div>
            </div>
            <div class="tab-pane" id="SMS">
              <div class="row">
                <div class="col-md-6">
                  <h4>API Username and Password</h4>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Email address</label>
                    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Password</label>
                    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Password">
                  </div>
                  <div class="form-group">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <button type="submit" class="btn btn-danger">Reset</button>
                  </div>

                </div>
              </div>
            </div>
            <div class="tab-pane" id="Tax">
              <div class="row">

              <div class="col-md-6">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Name On Bill</label>
                    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter...">
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Name On Liquor Bill</label>
                    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter...">
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group required">
                    <label>I Collect Tax *</label>
                    <select class="form-control select2" style="width: 100%;">
                      <option selected="selected">Yes</option>
                      <option>No</option>
                    </select>
                    <span class="help-block">This is required</span>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <label for="exampleInputEmail1">My Tax Title</label>
                    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter...">
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <label for="exampleInputEmail1">My Tax Registration No</label>
                    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter...">
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group ">
                    <label>My Tax is GST</label>
                    <select class="form-control select2" style="width: 100%;">
                      <option selected="selected">Yes</option>
                      <option>No</option>
                    </select>
                   
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group ">
                    <label>State Code </label>
                    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter...">
                   
                  </div>
                </div>

                <div class="col-md-12">
                  <div class="form-group ">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <button type="submit" class="btn btn-danger">Reset</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /.tab-content -->
    </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php
  require_once("incld/footer_incld.php");
  require_once("incld/sidebar_incld.php");
  ?>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
  </div>
  <!-- ./wrapper -->




  <!-- jQuery 2.2.3 -->
  <script src="<?= base_url("assets/adminlte") ?>/plugins/jQuery/jquery-2.2.3.min.js"></script>
  <!-- Bootstrap 3.3.6 -->
  <script src="<?= base_url("assets/adminlte") ?>/bootstrap/js/bootstrap.min.js"></script>
  <!-- SlimScroll -->
  <script src="<?= base_url("assets/adminlte") ?>/plugins/slimScroll/jquery.slimscroll.min.js"></script>
  <!-- FastClick -->
  <script src="<?= base_url("assets/adminlte") ?>/plugins/fastclick/fastclick.js"></script>
  <!-- AdminLTE App -->
  <script src="<?= base_url("assets/adminlte") ?>/dist/js/app.min.js"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="<?= base_url("assets/adminlte") ?>/dist/js/demo.js"></script>
  <!-- page script -->
  <script>
    $(function() {

    });
  </script>
</body>

</html>