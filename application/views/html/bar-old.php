<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Collapsed Sidebar Layout</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?= base_url("assets/adminlte") ?>/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url("assets/adminlte") ?>/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?= base_url("assets/adminlte") ?>/dist/css/skins/_all-skins.min.css">

  <!-- custom -->
  <link rel="stylesheet" href="<?= base_url("assets/adminlte") ?>/dist/css/custom.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<!-- ADD THE CLASS sidedar-collapse TO HIDE THE SIDEBAR PRIOR TO LOADING THE SITE -->

<body class="hold-transition skin-blue sidebar-collapse sidebar-mini">
  <!-- Site wrapper -->
  <div class="wrapper">

    <?php
    require_once("incld/header_incld.php");
    require_once("incld/menu_incld.php");
    ?>

    <!-- =============================================== -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
         Bar
        
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

          <li class="active">Bar</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <div class="row">
          <div class="col-md-3">
            <!-- Widget: user widget style 1 -->
            <div class="box box-widget">
              <!-- Add the bg color to the header using any of the bg-* classes -->
              <div class="bg-blue kitchenBox">

                <h5 class="pull-left">Invoice: A 000504</h5>
                <h5 class="pull-right">10:00</h5>
                <h5 class="tbNo">Table No: 4</h5>
              </div>
              <div class="cookorderitem">
                <ul class="nav nav-stacked">

                  <li><a href="JavaScript:void(0);">Pepsi (180ml) <span class="pull-right badge bg-green">Done</span></a></li>
                  <li><a href="JavaScript:void(0);">Blender Pride (1000ml)<span class="pull-right badge bg-yellow">In Progress</span></a></li>
                  <li><a href="JavaScript:void(0);">Vodka <span class="pull-right badge ">Not Ready</span></a></li>
                </ul>
              </div>
              <div class="box-footer">
                <button type="button" class="btn btn-default btn-sm"> Select All</button>
                <button type="button" class="btn btn-default btn-sm"> Done</button>
                <button type="button" class="btn btn-default btn-sm"> Prepare</button>
                
              </div>
            </div>
            <!-- /.widget-user -->
          </div>
          <div class="col-md-3">
            <!-- Widget: user widget style 1 -->
            <div class="box box-widget">
              <!-- Add the bg color to the header using any of the bg-* classes -->
              <div class="bg-blue kitchenBox">

                <h5 class="pull-left">Invoice: A 000504</h5>
                <h5 class="pull-right">10:00</h5>
                <h5 class="tbNo">Table No: 4</h5>
              </div>
              <div class="cookorderitem">
                <ul class="nav nav-stacked">

                  <li><a href="JavaScript:void(0);">Pepsi (180ml) <span class="pull-right badge bg-green">Done</span></a></li>
                  <li><a href="JavaScript:void(0);">Blender Pride (1000ml)<span class="pull-right badge bg-yellow">In Progress</span></a></li>
                  <li><a href="JavaScript:void(0);">Vodka <span class="pull-right badge ">Not Ready</span></a></li>
                </ul>
              </div>
              <div class="box-footer">
                <button type="button" class="btn btn-default btn-sm"> Select All</button>
                <button type="button" class="btn btn-default btn-sm"> Done</button>
                <button type="button" class="btn btn-default btn-sm"> Prepare</button>
                
              </div>
            </div>
            <!-- /.widget-user -->
          </div>
          <div class="col-md-3">
            <!-- Widget: user widget style 1 -->
            <div class="box box-widget">
              <!-- Add the bg color to the header using any of the bg-* classes -->
              <div class="bg-blue kitchenBox">

                <h5 class="pull-left">Invoice: A 000504</h5>
                <h5 class="pull-right">10:00</h5>
                <h5 class="tbNo">Table No: 4</h5>
              </div>
              <div class="cookorderitem">
                <ul class="nav nav-stacked">

                  <li><a href="JavaScript:void(0);">Pepsi (180ml) <span class="pull-right badge bg-green">Done</span></a></li>
                  <li><a href="JavaScript:void(0);">Blender Pride (1000ml)<span class="pull-right badge bg-yellow">In Progress</span></a></li>
                  <li><a href="JavaScript:void(0);">Vodka <span class="pull-right badge ">Not Ready</span></a></li>
                </ul>
              </div>
              <div class="box-footer">
                <button type="button" class="btn btn-default btn-sm"> Select All</button>
                <button type="button" class="btn btn-default btn-sm"> Done</button>
                <button type="button" class="btn btn-default btn-sm"> Prepare</button>
                
              </div>
            </div>
            <!-- /.widget-user -->
          </div>
          <div class="col-md-3">
            <!-- Widget: user widget style 1 -->
            <div class="box box-widget">
              <!-- Add the bg color to the header using any of the bg-* classes -->
              <div class="bg-blue kitchenBox">

                <h5 class="pull-left">Invoice: A 000504</h5>
                <h5 class="pull-right">10:00</h5>
                <h5 class="tbNo">Table No: 4</h5>
              </div>
              <div class="cookorderitem">
                <ul class="nav nav-stacked">

                  <li><a href="JavaScript:void(0);">Pepsi (180ml) <span class="pull-right badge bg-green">Done</span></a></li>
                  <li><a href="JavaScript:void(0);">Blender Pride (1000ml)<span class="pull-right badge bg-yellow">In Progress</span></a></li>
                  <li><a href="JavaScript:void(0);">Vodka <span class="pull-right badge ">Not Ready</span></a></li>
                </ul>
              </div>
              <div class="box-footer">
                <button type="button" class="btn btn-default btn-sm"> Select All</button>
                <button type="button" class="btn btn-default btn-sm"> Done</button>
                <button type="button" class="btn btn-default btn-sm"> Prepare</button>
                
              </div>
            </div>
            <!-- /.widget-user -->
          </div>
       
        
       
        </div>

      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <?php
    require_once("incld/footer_incld.php");
    require_once("incld/sidebar_incld.php");
    ?>
    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
  </div>
  <!-- ./wrapper -->

  <!-- jQuery 2.2.3 -->
  <script src="<?= base_url("assets/adminlte") ?>/plugins/jQuery/jquery-2.2.3.min.js"></script>
  <!-- Bootstrap 3.3.6 -->
  <script src="<?= base_url("assets/adminlte") ?>/bootstrap/js/bootstrap.min.js"></script>
  <!-- SlimScroll -->
  <script src="<?= base_url("assets/adminlte") ?>/plugins/slimScroll/jquery.slimscroll.min.js"></script>
  <!-- FastClick -->
  <script src="<?= base_url("assets/adminlte") ?>/plugins/fastclick/fastclick.js"></script>
  <!-- AdminLTE App -->
  <script src="<?= base_url("assets/adminlte") ?>/dist/js/app.min.js"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="<?= base_url("assets/adminlte") ?>/dist/js/demo.js"></script>

  <script>
    $(function() {


      $('.cookorderitem').slimScroll({
        height: '200px'
      });


    });
  </script>


</body>

</html>