<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Blank Page</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?= base_url("assets/adminlte") ?>/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?= base_url("assets/adminlte") ?>/plugins/datatables/dataTables.bootstrap.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?= base_url("assets/adminlte") ?>/plugins/select2/select2.min.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?= base_url("assets/adminlte") ?>/plugins/datepicker/datepicker3.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url("assets/adminlte") ?>/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?= base_url("assets/adminlte") ?>/dist/css/skins/_all-skins.min.css">
  <!-- custom -->
  <link rel="stylesheet" href="<?= base_url("assets/adminlte") ?>/dist/css/custom.css">


  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>

<body class="hold-transition skin-blue sidebar-mini">
  <!-- Site wrapper -->
  <div class="wrapper">

    <?php
    require_once("incld/header_incld.php");
    require_once("incld/menu_incld.php");
    ?>

    <!-- =============================================== -->





    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->

      <div class="row">
        <div class="col-md-4">
          <section class="content-header">
            <h1>Expense Report</h1>
            <ol class="breadcrumb horizontal">
              <li><a href="<?= base_url("html") ?>/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
              <li class="active">Expense Report</li>
            </ol>
          </section>
        </div>
        <div class="col-md-8">
          <ol class="right_btn">

          </ol>
        </div>
      </div>

      <section class="content">
        <div class="row">
          <div class="col-md-12">
            <div class="box">

              <div class="box-body">
                <div class="row">

                <div class="col-xs-2">
                    <div class="form-group">
                      <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" placeholder="Start" class="form-control pull-right" id="dStart">
                      </div>
                      <!-- /.input group -->
                    </div>
                  </div>

                  <div class="col-xs-2">
                    <div class="form-group">
                      <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" placeholder="End" class="form-control pull-right" id="dEnd">
                      </div>
                      <!-- /.input group -->
                    </div>
                  </div>

              

                  <div class="col-xs-2">
                    <div class="form-group">

                      <select class="form-control select2" style="width: 100%;">
                        <option>All Items</option>
                        <option> Gas Bill </option>
                        <option> Light Bill</option>

                      </select>
                    </div>
                  </div>

             
             

                  <div class="col-xs-1">
                    <button type="button" class="btn btn-primary">Search</button>
                  </div>



                </div>
              </div>
            </div>

          </div>


        </div>



        <div class="row">
          <div class="col-xs-12">
            <div class="box">
              <!-- /.box-header -->
              <div class="box-header with-border">
                <div class="col-xs-6">
                  <h4>Report (Date:01/11/2019 - 22/11/2019) </h4>
                </div>
                <div class="col-xs-6 text-right">
                  <button type="button" class="btn btn-primary">Print</button>
                  <button type="button" class="btn btn-primary">Excel</button>
                  <button type="button" class="btn btn-primary">PDF</button>
                </div>
              </div>
              <div class="box-body">



                <table id="example1" class="table table-bordered table-striped inventory_table">
                  <thead>
                    <tr>
                      <th width="30px">#</th>
                      <th>Date</th>
                      <th>Expense Items</th>
                      <th>Amount</th>
                      <th>Responsible Person</th>
                    </tr>
                  </thead>
                  <tbody>

                    <tr>
                      <td>1</td>
                      <td>06/05/2019</td>
                      <td>Light Bill</td>
                      <td>39800.00 </td>
                      <td>Smith</td>
                    </tr>
                    <tr>
                      <td>1</td>
                      <td>06/05/2019</td>
                      <td>Light Bill</td>
                      <td>39800.00 </td>
                      <td>Smith</td>
                    </tr>
                    <tr>
                      <td>1</td>
                      <td>06/05/2019</td>
                      <td>Light Bill</td>
                      <td>39800.00 </td>
                      <td>Smith</td>
                    </tr>
                    <tr>
                      <td>1</td>
                      <td>06/05/2019</td>
                      <td>Light Bill</td>
                      <td>39800.00 </td>
                      <td>Smith</td>
                    </tr>




                  </tbody>
                  <tfoot>
                    <tr>
                      <th>#</th>
                      <th> </th>
                      <th>Total</th>
                      <th>00</th>
                      <th></th>
                  
               
                    </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
          </div>
          <!-- /.col -->
        </div>










      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <?php
    require_once("incld/footer_incld.php");
    require_once("incld/sidebar_incld.php");
    ?>
    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
  </div>
  <!-- ./wrapper -->

  <!-- jQuery 2.2.3 -->
  <script src="<?= base_url("assets/adminlte") ?>/plugins/jQuery/jquery-2.2.3.min.js"></script>
  <!-- Bootstrap 3.3.6 -->
  <script src="<?= base_url("assets/adminlte") ?>/bootstrap/js/bootstrap.min.js"></script>
  <!-- DataTables -->
  <script src="<?= base_url("assets/adminlte") ?>/plugins/datatables/jquery.dataTables.min.js"></script>
  <script src="<?= base_url("assets/adminlte") ?>/plugins/datatables/dataTables.bootstrap.min.js"></script>
  <!-- Select2 -->
  <script src="<?= base_url("assets/adminlte") ?>/plugins/select2/select2.full.min.js"></script>
  <!-- SlimScroll -->
  <script src="<?= base_url("assets/adminlte") ?>/plugins/slimScroll/jquery.slimscroll.min.js"></script>
  <!-- bootstrap datepicker -->
  <script src="<?= base_url("assets/adminlte") ?>/plugins/datepicker/bootstrap-datepicker.js"></script>
  <!-- FastClick -->
  <script src="<?= base_url("assets/adminlte") ?>/plugins/fastclick/fastclick.js"></script>
  <!-- AdminLTE App -->
  <script src="<?= base_url("assets/adminlte") ?>/dist/js/app.min.js"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="<?= base_url("assets/adminlte") ?>/dist/js/demo.js"></script>
  <!-- page script -->

  <!-- page script -->
  <script src="<?= base_url("assets/adminlte") ?>/dist/js/bootstrap-multiselect.min.js"></script>
  <script>
    $(function() {
      $("#example1").DataTable();

      $(".select2").select2({
        placeholder: "Category",
      });

      $('#selectType, #selectmenu, #freemenu, #selectmenu1').multiselect({
        buttonWidth: '100%',
        includeSelectAllOption: true,
        nonSelectedText: 'Select an Option'
      });

      $('#dStart, #dEnd').datepicker({
        autoclose: true
      });

    });
  </script>
</body>

</html>