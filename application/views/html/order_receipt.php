<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Collapsed Sidebar Layout</title>

  <style>
    h1,h2,h3,h4,h5,h6{ margin:10px 0}
    table tr td { padding: 5px 0 ;}
    .print { width: 100%; background: #3c8dbc; text-align: center; padding: 10px; color: #fff; font-size: 18px; border: none; cursor: pointer }
  </style>
  
</head>


<body style="font-family:Arial, Helvetica, sans-serif;">
  <table style="width:400px; margin:0 auto">
    <tr>
      <td>
      <h5>Kitchen : Snacks</h5>
      <h5>Table : 4</h5>
      <h5>Time: 28-Nov-2019 5:30:44 PM</h5>
      <h5>KOT No. : 000001</h5>
    
      </td>
    </tr>

    <tr>
      <td style="border-top: 1px dashed #ccc">
      <table style="font-size:14px; width:100%">
        <tr>
        <td width="20%" style="border-bottom: 1px dashed #ccc" ><strong>Qty </strong></td>
          <td width="80%" style="border-bottom: 1px dashed #ccc"><strong>Description </strong></td>
        </tr>
        <tr>
          <td >1</td>
          <td >Paneer Tikka  </td>
        </tr>
    
        <tr>
          <td >1</td>
          <td >Paneer Tikka  </td>
        </tr>
    

        <tr>
          <td >1</td>
          <td >Paneer Tikka  </td>
        </tr>
    

        <tr>
          <td  colspan="2" style="border-top: 1px dashed #ccc" >
            Amount : 100.00

          </td>
          
        </tr>
        
      
        

        
      </table>
      </td>
    </tr>


    <tr>
      <td>
        <button class="print"> Print Now<button>
      </td>
    </tr>




  </table>


</body>

</html>