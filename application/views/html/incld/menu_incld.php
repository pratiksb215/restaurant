<?php

// dashboard
$dashboard = ($menu_active == "dashboard") ? "active" : "";

// Masters
$master_group = ($menu_active == "ingredient_categories" ||
  $menu_active == "ingredient_units" ||
  $menu_active == "ingredients" ||
  $menu_active == "liquor" ||
  $menu_active == "vats"  ||
  $menu_active == "recipes_categories" ||
  $menu_active == "recipes" ||
  $menu_active == "base" ||
  $menu_active == "new_recipe" ||
  $menu_active == "menus"  ||
  $menu_active == "suppliers"  ||
  $menu_active == "customers"  ||
  $menu_active == "expense_items" ||
  $menu_active == "payment_methods" ||
  $menu_active == "waiters" ||
  $menu_active == "tables" ||
  $menu_active == "offers" ||
  $menu_active == "new_offer" ||
  $menu_active == "rewards_points" ||
  $menu_active == "additional_charges" ||
  $menu_active == "add_rewards_points") ? "active" : "";





$Ingredient_Categories = ($menu_active == "ingredient_categories") ? "active" : "";
$Ingredient_Units = ($menu_active == "ingredient_units") ? "active" : "";
$Ingredients = ($menu_active == "ingredients") ? "active" : "";
$liquor = ($menu_active == "liquor") ? "active" : "";
$VATs = ($menu_active == "vats") ? "active" : "";
$Recipes_Categories = ($menu_active == "recipes_categories") ? "active" : "";
$Recipes = ($menu_active == "recipes" || $menu_active == "new_recipe") ? "active" : "";
$base = ($menu_active == "base") ? "active" : "";
$Menus = ($menu_active == "menus") ? "active" : "";
$Suppliers = ($menu_active == "suppliers") ? "active" : "";
$Customers = ($menu_active == "customers") ? "active" : "";
$Expense_Items = ($menu_active == "expense_items") ? "active" : "";
$Payment_Methods = ($menu_active == "payment_methods") ? "active" : "";
$waiters = ($menu_active == "waiters") ? "active" : "";
$Tables = ($menu_active == "tables") ? "active" : "";
$Offers = ($menu_active == "offers" || $menu_active == "new_offer") ? "active" : "";

$additional_charges = ($menu_active == "additional_charges") ? "active" : "";


$Rewards_Points = ($menu_active == "rewards_points" || $menu_active == "add_rewards_points") ? "active" : "";



$outlets = ($menu_active == "outlets") ? "active" : "";

$users = ($menu_active == "users") ? "active" : "";
$attendance = ($menu_active == "attendance") ? "active" : "";
$expenses = ($menu_active == "expenses") ? "active" : "";

$order_group = ($menu_active == "place_order" ||
$menu_active == "place_order_admin" ||
  $menu_active == "kitchen" ||
  $menu_active == "bar") ? "active" : "";

$placeorder = ($menu_active == "place_order") ? "active" : "";
$kitchen = ($menu_active == "kitchen") ? "active" : "";
$bar = ($menu_active == "bar") ? "active" : "";

$placeorderadmin = ($menu_active == "place_order_admin") ? "active" : "";

$purchases = ($menu_active == "purchases" || $menu_active == "new_purchase" || $menu_active == "purchase_details") ? "active" : "";

$inventory = ($menu_active == "inventory" || $menu_active == "inventory_alert" || $menu_active == "inventory_adjustment" || $menu_active == "new_adjustment" || $menu_active == "adjustment_details") ? "active" : "";



$report_group = (
  $menu_active == "report_sales" ||
  $menu_active == "report_raw"||
  $menu_active == "supplier_report"||
  $menu_active == "report_expense"||
  $menu_active == "report_purchase" ||
  $menu_active == "report_attendance" 
  ) ? "active" : "";

  $report_sales = ($menu_active == "report_sales") ? "active" : "";
  $report_raw = ($menu_active == "report_raw") ? "active" : "";
  $supplier_report = ($menu_active == "supplier_report") ? "active" : "";
  $report_expense = ($menu_active == "report_expense") ? "active" : "";
  $report_purchase = ($menu_active == "report_purchase") ? "active" : "";
  $report_attendance = ($menu_active == "report_attendance") ? "active" : "";


  
?>

<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="<?= base_url("assets/adminlte") ?>/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p>Alexander Pierce</p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>
    <!-- search form -->
    <form action="#" method="get" class="sidebar-form">
      <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Search...">
        <span class="input-group-btn">
          <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
          </button>
        </span>
      </div>
    </form>
    <!-- /.search form -->
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
      <li class="header">MAIN NAVIGATION</li>
      <li class="<?= $dashboard ?> ">
        <a href="<?= base_url("html") ?>/dashboard">
          <i class="fa fa-dashboard"></i> <span>Dashboard</span>
        </a>
      </li>

      <li class="<?= $order_group ?> treeview">
        <a href="#">
          <i class="fa fa-rupee"></i>
          <span>Orders</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li class="<?= $placeorder ?> ">
            <a href="<?= base_url("html") ?>/place_order">
              <i class="fa  fa-circle-o"></i> <span>Place Order Waiter</span>
            </a>
          </li>
          <li class="<?= $placeorderadmin ?> ">
            <a href="<?= base_url("html") ?>/place_order_admin">
              <i class="fa  fa-circle-o"></i> <span>Place Order admin</span>
            </a>
          </li>

          <li class="<?= $kitchen ?> ">
            <a href="<?= base_url("html") ?>/kitchen">
              <i class="fa  fa-circle-o"></i> <span>Kitchen</span>
            </a>
          </li>

          <li class="<?= $bar ?> ">
            <a href="<?= base_url("html") ?>/bar">
              <i class="fa  fa-circle-o"></i> <span>Bar</span>
            </a>
          </li>


        </ul>
      <li class="<?= $master_group ?> treeview">
        <a href="#">
          <i class="fa fa-list"></i>
          <span>Masters</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>

        <ul class="treeview-menu">
          <li class="<?= $Ingredient_Categories ?>">
            <a href="<?= base_url("html") ?>/ingredient_categories"><i class="fa fa-circle-o"></i>Ingredient Categories</a>
          </li>
          <li class="<?= $Ingredient_Units ?>">
            <a href="<?= base_url("html") ?>/ingredient_units"><i class="fa fa-circle-o"></i>Ingredient Units</a>
          </li>
          <li class="<?= $Ingredients ?>">
            <a href="<?= base_url("html") ?>/ingredients"><i class="fa fa-circle-o"></i>Ingredient</a>
          </li>
          <li class="<?= $liquor ?>">
            <a href="<?= base_url("html") ?>/liquor"><i class="fa fa-circle-o"></i>Liquor</a>
          </li>
          <li class="<?= $VATs ?>">
            <a href="<?= base_url("html") ?>/vats"><i class="fa fa-circle-o"></i>VATs</a>
          </li>
          <li class="<?= $base ?>">
            <a href="<?= base_url("html") ?>/base"><i class="fa fa-circle-o"></i>Base</a>
          </li>
          <li class="<?= $Recipes_Categories ?>">
            <a href="<?= base_url("html") ?>/recipes_categories"><i class="fa fa-circle-o"></i>Recipes Categories</a>
          </li>
          <li class="<?= $Recipes ?>">
            <a href="<?= base_url("html") ?>/recipes"><i class="fa fa-circle-o"></i>Recipes</a>
          </li>
          <li class="<?= $Menus ?>">
            <a href="<?= base_url("html") ?>/menus"><i class="fa fa-circle-o"></i>Menus</a>
          </li>
          <li class="<?= $Suppliers ?>">
            <a href="<?= base_url("html") ?>/suppliers"><i class="fa fa-circle-o"></i>Suppliers</a>
          </li>
          <li class="<?= $Customers ?>">
            <a href="<?= base_url("html") ?>/customers"><i class="fa fa-circle-o"></i>Guest</a>
          </li>
          <li class="<?= $Expense_Items ?>">
            <a href="<?= base_url("html") ?>/expense_items"><i class="fa fa-circle-o"></i>Expense Items</a>
          </li>
          <li class="<?= $Payment_Methods ?>">
            <a href="<?= base_url("html") ?>/payment_methods"><i class="fa fa-circle-o"></i>Payment Methods</a>
          </li>
          <li class="<?= $waiters ?>">
            <a href="<?= base_url("html") ?>/waiters"><i class="fa fa-circle-o"></i>Manage Waiters</a>
          </li>
          <li class="<?= $Tables ?>">
            <a href="<?= base_url("html") ?>/tables"><i class="fa fa-circle-o"></i>Tables</a>
          </li>
          <li class="<?= $Offers ?>">
            <a href="<?= base_url("html") ?>/offers"><i class="fa fa-circle-o"></i>Offers</a>
          </li>
          <li class="<?= $Rewards_Points ?>">
            <a href="<?= base_url("html") ?>/add_rewards_points"><i class="fa fa-circle-o"></i>Rewards Points</a>
          </li>
          <li class="<?= $additional_charges ?>">
            <a href="<?= base_url("html") ?>/additional_charges"><i class="fa fa-circle-o"></i>Additional Charges</a>
          </li>


        </ul>
      </li>
      <li class="<?= $outlets ?> ">
        <a href="<?= base_url("html") ?>/outlets">
          <i class="fa fa-cutlery"></i> <span>Outlets</span>
        </a>
      </li>
      <li class="<?= $users ?> ">
        <a href="<?= base_url("html") ?>/users">
          <i class="fa fa-users"></i> <span>Users</span>
        </a>
      </li>
      <li class="<?= $attendance ?> ">
        <a href="<?= base_url("html") ?>/attendance">
          <i class="fa fa-calendar"></i> <span>Attendance</span>
        </a>
      </li>
      <li class="<?= $expenses ?> ">
        <a href="<?= base_url("html") ?>/expenses">
          <i class="fa  fa-rupee"></i> <span>Expenses</span>
        </a>
      </li>
      <li class="<?= $purchases ?> ">
        <a href="<?= base_url("html") ?>/purchases">
          <i class="fa  fa-money"></i> <span>Purchases</span>
        </a>
      </li>

      <li class="<?= $inventory ?> ">
        <a href="<?= base_url("html") ?>/inventory">
          <i class="fa  fa-list-alt"></i> <span>Inventory</span>
        </a>
      </li>

      <li class="<?= $report_group ?> treeview">
        <a href="#">
          <i class="fa fa-list"></i>
          <span>Report</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>

        <ul class="treeview-menu">
          <li class="<?= $report_sales ?>">
            <a href="<?= base_url("html") ?>/report_sales"><i class="fa fa-circle-o"></i>Sales Report</a>
          </li>

          <li class="<?= $report_raw ?>">
            <a href="<?= base_url("html") ?>/report_raw"><i class="fa fa-circle-o"></i>Raw Items Report</a>
          </li>

          <li class="<?= $supplier_report ?>">
            <a href="<?= base_url("html") ?>/supplier_report"><i class="fa fa-circle-o"></i>Supplier Report</a>
          </li>

          <li class="<?= $report_expense ?>">
            <a href="<?= base_url("html") ?>/report_expense"><i class="fa fa-circle-o"></i>Expense Report</a>
          </li>

          <li class="<?= $report_purchase ?>">
            <a href="<?= base_url("html") ?>/report_purchase"><i class="fa fa-circle-o"></i>Purchase Report</a>
          </li>

          <li class="<?= $report_attendance ?>">
            <a href="<?= base_url("html") ?>/report_attendance"><i class="fa fa-circle-o"></i>Attendance Report</a>
          </li>
     
          
        </ul>
      </li>


    </ul>
  </section>
  <!-- /.sidebar -->
</aside>