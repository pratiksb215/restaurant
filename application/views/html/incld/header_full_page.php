<?php 

$place_order_admin = ($menu_active == "place_order_admin") ;

?>
    <header class="main-header">
      <nav class="navbar navbar-static-top full-page-header">

        <div class="navbar-header">
          <a href="<?= base_url("assets/adminlte") ?>/index2.html" class="navbar-brand"><b>Admin</b>LTE</a>
         
        </div>

        <div class="navbar-custom-menu">
     
          <ul class="nav navbar-nav">
         
            <!-- User Account: style can be found in dropdown.less -->
            <li class="dropdown user user-menu">
              <a href="#">
                <img src="<?= base_url("assets/adminlte") ?>/dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
                <span class="hidden-xs">Alexander Pierce</span>
              </a>
              

              <?php if($place_order_admin )  {?>
              <li class="logout" style="width:200px" >
            <div class="form-group required">
                        <select class="form-control select2drp" style="width: 100%;">
                          <option selected="selected">Select Waiter</option>
                          <option>John</option>
                          <option>Smith</option>
                          <option>Sam</option>
                        </select>
                        <span class="help-block">This is required</span>
                      </div>
            </li>
<?php } ?>

            </li>
            <li class="logout">
              <button type="button" class="btn btn-default btn-md">Back</button>
            </li>
            <li class="logout">
              <button type="button" class="btn btn-default btn-md"><i class="fa fa-sign-out"></i> Logout</button>
            </li>
          </ul>
        </div>

        <!-- /.container-fluid -->
      </nav>
    </header>