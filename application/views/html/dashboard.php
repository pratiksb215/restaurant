<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?= base_url("assets/adminlte") ?>/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url("assets/adminlte") ?>/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?= base_url("assets/adminlte") ?>/dist/css/skins/_all-skins.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?= base_url("assets/adminlte") ?>/plugins/iCheck/flat/blue.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="<?= base_url("assets/adminlte") ?>/plugins/morris/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?= base_url("assets/adminlte") ?>/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="<?= base_url("assets/adminlte") ?>/plugins/datepicker/datepicker3.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?= base_url("assets/adminlte") ?>/plugins/daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?= base_url("assets/adminlte") ?>/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <!-- custom -->
  <link rel="stylesheet" href="<?= base_url("assets/adminlte") ?>/dist/css/custom.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <?php
    require_once("incld/header_incld.php");
    require_once("incld/menu_incld.php");
    ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Dashboard
        
        </h1>
        <ol class="breadcrumb">
          <li><a href="<?= base_url("html") ?>/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Dashboard</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
              <div class="inner">
                <h3>150</h3>

                <p>Orders</p>
              </div>
              <div class="icon">
                <i class="ion ion-bag"></i>
              </div>
           
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
              <div class="inner">
                <h3><sup style="font-size: 20px">Rs </sup>1,34,775</h3>

                <p>Revenue</p>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
              
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
              <div class="inner">
                <h3>11,345</h3>

                <p>Guest</p>
              </div>
              <div class="icon">
                <i class="ion ion-person-add"></i>
              </div>
              
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red">
              <div class="inner">
                <h3>1,34,534</h3>

                <p>Raw items</p>
              </div>
              <div class="icon">
                <i class="ion ion-pie-graph"></i>
              </div>
             
            </div>
          </div>
          <!-- ./col -->
        </div>
        <!-- /.row -->
        <!-- Main row -->
        <div class="row">
          <!-- Left col -->
          <section class="col-lg-7 connectedSortable">

          
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Sales</h3>

              <div class="box-tools pull-right">
                
                <button type="button" class="btn btn-primary btn-sm daterange pull-right"  title="Date range">
                    <i class="fa fa-calendar"></i></button>
              </div>
            </div>
            <div class="box-body">
           
            <canvas id="sales" style="height:400px"></canvas>
          
            </div>
            <!-- /.box-body -->
          </div>



          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Running Orders</h3>

              <div class="box-tools pull-right">
             
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

              <div class="table-responsive" >
              <table class="table no-margin">
                  <tr>
                    <th width="15%">Order NO</th>
                    <th width="10%">Table</th>
                    <th width="20%"> Waiter</th>
                    <th width="20%"> Deatils</th>
  
                  </tr>
                  </table>
                <div id="curreneOrder">
                <table class="table no-margin">
            
                  <tbody >
                  <tr>
                    <td  width="15%"><a href="<?= base_url("html") ?>/place_order_admin">OR9842</a></td>
                    <td width="10%">4</td>

                    <td width="20%">John </td>
                    <td width="20%">Panner Tikka, Rice ... </td>
                  </tr>

                  <tr>
                  <td  width="15%"><a href="<?= base_url("html") ?>/place_order_admin">OR9842</a></td>
                    <td width="10%">4</td>
                    <td width="20%">John </td>
                    <td width="20%">Panner Tikka, Rice ... </td>
                  </tr>

                  <tr>
                  <td  width="15%"><a href="<?= base_url("html") ?>/place_order_admin">OR9842</a></td>
                    <td width="10%">4</td>
                    <td width="20%">John </td>
                    <td width="20%">Panner Tikka, Rice ... </td>
                  </tr>

                  <tr>
                  <td  width="15%"><a href="<?= base_url("html") ?>/place_order_admin">OR9842</a></td>
                    <td width="10%">4</td>
                    <td width="20%">John </td>
                    <td width="20%">Panner Tikka, Rice ... </td>
                  </tr>


                  <tr>
                  <td  width="15%"><a href="<?= base_url("html") ?>/place_order_admin">OR9842</a></td>
                    <td width="10%">4</td>
                    <td width="20%">John </td>
                    <td width="20%">Panner Tikka, Rice ... </td>
                  </tr>

                  <tr>
                  <td  width="15%"><a href="<?= base_url("html") ?>/place_order_admin">OR9842</a></td>
                    <td width="10%">4</td>
                    <td width="20%">John </td>
                    <td width="20%">Panner Tikka, Rice ... </td>
                  </tr>


                  <tr>
                  <td  width="15%"><a href="<?= base_url("html") ?>/place_order_admin">OR9842</a></td>
                    <td width="10%">4</td>
                    <td width="20%">John </td>
                    <td width="20%">Panner Tikka, Rice ... </td>
                  </tr>


                  <tr>
                  <td  width="15%"><a href="<?= base_url("html") ?>/place_order_admin">OR9842</a></td>
                    <td width="10%">4</td>
                    <td width="20%">John </td>
                    <td width="20%">Panner Tikka, Rice ... </td>
                  </tr>


                  <tr>
                  <td  width="15%"><a href="<?= base_url("html") ?>/place_order_admin">OR9842</a></td>
                    <td width="10%">4</td>
                    <td width="20%">John </td>
                    <td width="20%">Panner Tikka, Rice ... </td>
                  </tr>
                 
                  </tbody>
                </table>
                </div>

              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
              <a href="<?= base_url("html") ?>/place_order_admin" class="btn btn-sm btn-info btn-flat pull-left">Place New Order</a>
             
            </div>
            <!-- /.box-footer -->
          </div>


          </section>
          <!-- /.Left col -->
          <!-- right col (We are only adding the ID to make the widgets sortable)-->
          <section class="col-lg-5 connectedSortable">

          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Orders</h3>

              <div class="box-tools pull-right">
               
              <button type="button" class="btn btn-primary btn-sm daterange pull-right"  title="Date range">
                    <i class="fa fa-calendar"></i></button>
              </div>
            </div>
            <div class="box-body">
            <canvas style="height:320px" id="orderDoughnut"></canvas>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->



             <!-- PRODUCT LIST -->
             <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Top Selling Items</h3>

              <div class="box-tools pull-right">
               
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body" id=topItems>
              <ul class="products-list product-list-in-box">
                <li class="item">
                  <div class="product-img">
                    <img src="<?= base_url("assets/adminlte") ?>/dist/img/default-50x50.gif" alt="Product Image">
                  </div>
                  <div class="product-info">
                    <abbr href="javascript:void(0)" class="product-title">Paneer Tikka 
                      <span class="label label-warning pull-right">$1800</span></abbr>
                        <span class="product-description">
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                        </span>
                  </div>
                </li>

                <li class="item">
                  <div class="product-img">
                    <img src="<?= base_url("assets/adminlte") ?>/dist/img/default-50x50.gif" alt="Product Image">
                  </div>
                  <div class="product-info">
                    <abbr href="javascript:void(0)" class="product-title">Paneer Tikka 
                      <span class="label label-warning pull-right">$1800</span></abbr>
                        <span class="product-description">
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                        </span>
                  </div>
                </li>

                <li class="item">
                  <div class="product-img">
                    <img src="<?= base_url("assets/adminlte") ?>/dist/img/default-50x50.gif" alt="Product Image">
                  </div>
                  <div class="product-info">
                    <abbr href="javascript:void(0)" class="product-title">Paneer Tikka 
                      <span class="label label-warning pull-right">$1800</span></abbr>
                        <span class="product-description">
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                        </span>
                  </div>
                </li>


                <li class="item">
                  <div class="product-img">
                    <img src="<?= base_url("assets/adminlte") ?>/dist/img/default-50x50.gif" alt="Product Image">
                  </div>
                  <div class="product-info">
                    <abbr href="javascript:void(0)" class="product-title">Paneer Tikka 
                      <span class="label label-warning pull-right">$1800</span></abbr>
                        <span class="product-description">
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                        </span>
                  </div>
                </li>


                <li class="item">
                  <div class="product-img">
                    <img src="<?= base_url("assets/adminlte") ?>/dist/img/default-50x50.gif" alt="Product Image">
                  </div>
                  <div class="product-info">
                    <abbr href="javascript:void(0)" class="product-title">Paneer Tikka 
                      <span class="label label-warning pull-right">$1800</span></abbr>
                        <span class="product-description">
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                        </span>
                  </div>
                </li>


                <li class="item">
                  <div class="product-img">
                    <img src="<?= base_url("assets/adminlte") ?>/dist/img/default-50x50.gif" alt="Product Image">
                  </div>
                  <div class="product-info">
                    <abbr href="javascript:void(0)" class="product-title">Paneer Tikka 
                      <span class="label label-warning pull-right">$1800</span></abbr>
                        <span class="product-description">
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                        </span>
                  </div>
                </li>


       
              </ul>
            </div>
            <!-- /.box-body -->
            <div class="box-footer text-center">
              <a href="<?= base_url("html") ?>/menus" class="uppercase">View All Products</a>
            </div>
            <!-- /.box-footer -->
          </div>

          </section>
          <!-- right col -->
        </div>
        <!-- /.row (main row) -->

      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <?php
    require_once("incld/footer_incld.php");
    require_once("incld/sidebar_incld.php");
    ?>

    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
  </div>
  <!-- ./wrapper -->

  <!-- jQuery 2.2.3 -->
  <script src="<?= base_url("assets/adminlte") ?>/plugins/jQuery/jquery-2.2.3.min.js"></script>
  <!-- jQuery UI 1.11.4 -->
  <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
  <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
  <script>
    $.widget.bridge('uibutton', $.ui.button);
  </script>
  <!-- Bootstrap 3.3.6 -->
  <script src="<?= base_url("assets/adminlte") ?>/bootstrap/js/bootstrap.min.js"></script>


  <!-- daterangepicker -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
  <script src="<?= base_url("assets/adminlte") ?>/plugins/daterangepicker/daterangepicker.js"></script>
  <!-- datepicker -->
  <script src="<?= base_url("assets/adminlte") ?>/plugins/datepicker/bootstrap-datepicker.js"></script>
  <!-- chartjs -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.bundle.min.js"></script>
  
  <!-- Slimscroll -->
  <script src="<?= base_url("assets/adminlte") ?>/plugins/slimScroll/jquery.slimscroll.min.js"></script>
  <script src="<?= base_url("assets/adminlte") ?>/dist/js/app.min.js"></script>
  <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
  <script src="<?= base_url("assets/adminlte") ?>/dist/js/pages/dashboard-custom.js"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="<?= base_url("assets/adminlte") ?>/dist/js/demo.js"></script>

  <script>
  $(function () {
  
    var color = {
       red : '#ff6384',
       blue : '#36a2eb',
       yellow : '#ffd166',
       green : '#4bc0c0',
       purple : '#9966ff',
       white : '#ffffff',
   
     }

 var ctx = document.getElementById('sales').getContext("2d");

var myChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL"],
        datasets: [{
            label: "Sale",
            borderColor:  color.red,
            pointBorderColor: color.white,
            pointBackgroundColor:  color.red,
            fill: false,
            pointBorderWidth: 2,
            borderWidth: 4,
            pointRadius: 5,
            data: [50, 70, 150, 356, 567, 866, 1098]
        },{

        label: "Order",
            borderColor:  color.blue,
            pointBorderColor: color.white,
            pointBackgroundColor:  color.blue,
            fill: false,
            pointBorderWidth: 2,
            borderWidth: 4,
            pointRadius: 5,
            data: [10, 45, 256, 245, 456, 1000, 606]
        }
      ]
    },
    options: {
        legend: {
            position: "bottom"
        },
        title: {
            display: true,
            text: 'Last 30 days',
            fontSize: 18,
            lineHeight: 2
        }
   
    }
});




var orderDoughnut = document.getElementById("orderDoughnut").getContext('2d');

var orderDoughnut = new Chart(orderDoughnut, {
    type: 'doughnut',
    data: {
        labels: ["Dine In",	"Delivery",	"Take Away"],
        datasets: [{    
            data: [3056,	1598,	2424,], // Specify the data values array
           // borderColor: [ ], // Add custom color border 
            backgroundColor: [color.red, color.yellow, color.blue ], // Add custom color background (Points and Fill)
            borderWidth: 1 // Specify bar border width
        }]},         
    options: {
      responsive: true, // Instruct chart js to respond nicely.
      maintainAspectRatio: false, // Add to prevent default behaviour of full-width/height 
      legend: {
            position: "bottom"
        },
        title: {
            display: true,
            text: 'Last 30 days',
            fontSize: 18,
            lineHeight: 2
        },
        
    }
});

  });
</script>

</body>

</html>