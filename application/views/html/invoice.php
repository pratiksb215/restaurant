<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Collapsed Sidebar Layout</title>

  <style>
    h1,
    h2,
    h3,
    h4,
    h5,
    h6 {
      margin: 10px 0
    }

    table tr td {
      padding: 5px 0;
    }

    .print {
      width: 100%;
      background: #3c8dbc;
      text-align: center;
      padding: 10px;
      color: #fff;
      font-size: 18px;
      border: none;
      cursor: pointer
    }
  </style>

</head>


<body style="font-family:Arial, Helvetica, sans-serif;">

    <table style="width:400px; margin:0 auto">
      <tr>
        <td>
            <table style="width:100%; margin:0 auto">
                <tr>
                  <td style="text-align:center">
                    <h2>Restaurant Name</h2>
                    <h5>1105 Sardis Station LAKE CITY Florida</h5>
                    <h5>612-437-4651 / 386-623-7701</h5>
                    <h4>Invoice</h4>
                  </td>
                </tr>
                <tr>
                  <td>
            
                    <table width='100%'>
                      <tr>
                        <td width="50%">
                          <h5>No: F-23421</h5>
                          <h5>Tb: xyz</h5>
                          <h5>Wt Smith </h5>
                        </td>
                        <td width="50%" style="text-align: right;">
                          <h5>Dt: 02/10/2019 14:52</h5>
                          <h5>Px: 4</h5>
                          <h5>Op: John </h5>
                        </td>
                      </tr>
                    </table>
            
                  </td>
                </tr>
                <tr>
                  <td style="font-size:13px; padding:10px 0;  border-bottom: 1px dashed #ccc;  border-top: 1px dashed #ccc" >
                    KOT : 10023, 10045, 10089
                  </td>
                </tr>
                <tr>
                  <td>
                    <table style="font-size:14px; width:100%">
                      <tr>
                          <th style="padding: 0 0 10px 0; text-align: left; border-bottom: 1px dashed #ccc">Description </th>
                          <th style="padding: 0 0 10px 0; text-align: center; width: 50px; border-bottom: 1px dashed #ccc">Qty. </th>
                          <th style="padding: 0 0 10px 0;  text-align: right; width: 100px; border-bottom: 1px dashed #ccc">Amt. </th>
                      </tr>
                      <tr >
                        <td>Panner Tikka </td>
                        <td style="text-align: center;">1</td>
                        <td style="text-align: right;">Rs. 150.00</td>
                      </tr>
                      <tr >
                          <td>Panner Tikka </td>
                          <td style="text-align: center;">1</td>
                          <td style="text-align: right;">Rs. 150.00</td>
                        </tr>
                        <tr >
                            <td>Panner Tikka </td>
                            <td style="text-align: center;">1</td>
                            <td style="text-align: right;">Rs. 150.00</td>
                          </tr>
                          <tr >
                              <td>Panner Tikka </td>
                              <td style="text-align: center;">1</td>
                              <td style="text-align: right;">Rs. 150.00</td>
                            </tr>
                            <tr >
                                <td>Panner Tikka </td>
                                <td style="text-align: center;">1</td>
                                <td style="text-align: right;">Rs. 150.00</td>
                              </tr>
                 
            
                    </table>
                  </td>
                </tr>
            
                <tr>
                  <td style="border-top: 1px dashed #ccc">
                    <table style="font-size:14px; width:100%">
                      <tr>
                        <td width="80%" style="text-align: right;">Food Taxable Total : </td>
                        <td width="20%" style="text-align: right;"> Rs. 650.00</td>
                      </tr>
                      <tr>
                        <td width="80%" style="text-align: right;">2.5% SGST on Food Taxable : </td>
                        <td width="20%" style="text-align: right;"> Rs. 50.00</td>
                      </tr>
            
                      <tr>
                        <td width="80%" style="text-align: right;">2.5% CGST on Food Taxable : </td>
                        <td width="20%" style="text-align: right;"> Rs. 50.00</td>
                      </tr>
                    </table>
                  </td>
                </tr>
            
                <tr>
                  <td style="border-top: 1px dashed #ccc">
                    <table style="font-size:14px; width:100%">
                      <tr>
                        <td width="80%" style="text-align: right;">Sub Total : </td>
                        <td width="20%" style="text-align: right;"> Rs.650.00</td>
                      </tr>
                    </table>
                  </td>
                </tr>
            
                <tr>
                  <td style="border-top: 1px dashed #ccc">
                    <table style="font-size:14px; width:100%">
                      <tr>
                        <td width="80%" style="text-align: right;">Disc Amt (%) : </td>
                        <td width="20%" style="text-align: right;">Rs. 0.00</td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr>
                  <td style="border-top: 1px dashed #ccc">
                    <table style="font-size:14px; width:100%">
                      <tr>
                        <td width="80%" style="text-align: right;"><strong>Bill Total : </strong></td>
                        <td width="20%" style="text-align: right;"><strong> Rs. 650.00</strong></td>
                      </tr>
                    </table>
                  </td>
                </tr>
            
                <tr>
                  <td style="font-size:14px">
                    GST No. : 23JYJ575HR67H6H8
                  </td>
                </tr>
                <tr>
                  <td style="font-size:14px">
                    Thank You!
                  </td>
                </tr>
               
            
              </table>
        </td>
      </tr>
      <tr>
          <td>
              <table style="width:100%; margin:80px 0 0 0">
                  <tr>
                    <td style="text-align:center">
                      <h2>Restaurant Name</h2>
                      <h5>1105 Sardis Station LAKE CITY Florida</h5>
                      <h5>612-437-4651 / 386-623-7701</h5>
                      <h4>Invoice</h4>
                    </td>
                  </tr>
                  <tr>
                    <td>
              
                      <table width='100%'>
                        <tr>
                          <td width="50%">
                            <h5>No: F-23421</h5>
                            <h5>Tb: xyz</h5>
                            <h5>Wt Smith </h5>
                          </td>
                          <td width="50%" style="text-align: right;">
                            <h5>Dt: 02/10/2019 14:52</h5>
                            <h5>Px: 4</h5>
                            <h5>Op: John </h5>
                          </td>
                        </tr>
                      </table>
              
                    </td>
                  </tr>
                  <tr>
                  <td style="font-size:13px; padding:10px 0;  border-bottom: 1px dashed #ccc;  border-top: 1px dashed #ccc" >
                    KOT : 10023, 10045, 10089
                  </td>
                </tr>
                  <tr>
                    <td>
                      <table style="font-size:14px; width:100%">
                        <tr>
                            <th style="padding: 0 0 10px 0; text-align: left; border-bottom: 1px dashed #ccc">Description </th>
                            <th style="padding: 0 0 10px 0; text-align: center; width: 50px; border-bottom: 1px dashed #ccc">Qty. </th>
                            <th style="padding: 0 0 10px 0;  text-align: right; width: 100px; border-bottom: 1px dashed #ccc">Amt. </th>
                        </tr>
                        <tr >
                          <td>Beer </td>
                          <td style="text-align: center;">1</td>
                          <td style="text-align: right;">Rs. 150.00</td>
                        </tr>
                        <tr >
                            <td>Vodka </td>
                            <td style="text-align: center;">1</td>
                            <td style="text-align: right;">Rs. 150.00</td>
                          </tr>
                         
              
                      </table>
                    </td>
                  </tr>
              
                  <tr>
                    <td style="border-top: 1px dashed #ccc">
                      <table style="font-size:14px; width:100%">
                        <tr>
                          <td width="80%" style="text-align: right;">Food Taxable Total : </td>
                          <td width="20%" style="text-align: right;"> Rs. 650.00</td>
                        </tr>
                        <tr>
                          <td width="80%" style="text-align: right;">2.5% SGST on Food Taxable : </td>
                          <td width="20%" style="text-align: right;"> Rs. 50.00</td>
                        </tr>
              
                        <tr>
                          <td width="80%" style="text-align: right;">2.5% CGST on Food Taxable : </td>
                          <td width="20%" style="text-align: right;"> Rs. 50.00</td>
                        </tr>
                      </table>
                    </td>
                  </tr>
              
                  <tr>
                    <td style="border-top: 1px dashed #ccc">
                      <table style="font-size:14px; width:100%">
                        <tr>
                          <td width="80%" style="text-align: right;">Sub Total : </td>
                          <td width="20%" style="text-align: right;"> Rs.650.00</td>
                        </tr>
                      </table>
                    </td>
                  </tr>
              
                  <tr>
                    <td style="border-top: 1px dashed #ccc">
                      <table style="font-size:14px; width:100%">
                        <tr>
                          <td width="80%" style="text-align: right;">Disc Amt (%) : </td>
                          <td width="20%" style="text-align: right;">Rs. 0.00</td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                  <tr>
                    <td style="border-top: 1px dashed #ccc">
                      <table style="font-size:14px; width:100%">
                        <tr>
                          <td width="80%" style="text-align: right;"><strong>Bill Total : </strong></td>
                          <td width="20%" style="text-align: right;"><strong> Rs. 650.00</strong></td>
                        </tr>
                      </table>
                    </td>
                  </tr>
              
                  <tr>
                    <td style="font-size:14px">
                      GST No. : 23JYJ575HR67H6H8
                    </td>
                  </tr>
                  <tr>
                    <td style="font-size:14px">
                      Thank You!
                    </td>
                  </tr>
                
              
                </table>
          </td>
        </tr>

        <tr>
          <td style="padding: 20px 0 0 0; Text-align:right" >
           <h3>Payable Total : $1200 </h3>
          </td>
        </tr>
        <tr>
          <td style=" Text-align:right" >
           <h5>Split Bill : $600 For 2 Guest</h5>
     
          </td>
        </tr>
        <tr>
          <td style="padding: 20px 0">
            <button class="print">Print</button>
          </td>
        </tr>
        </table>



</body>

</html>