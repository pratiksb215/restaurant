<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Blank Page</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?= base_url("assets/adminlte") ?>/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?= base_url("assets/adminlte") ?>/plugins/select2/select2.min.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="<?= base_url("assets/adminlte") ?>/plugins/datepicker/datepicker3.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url("assets/adminlte") ?>/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?= base_url("assets/adminlte") ?>/dist/css/skins/_all-skins.min.css">
  <!-- custom -->
  <link rel="stylesheet" href="<?= base_url("assets/adminlte") ?>/dist/css/custom.css">


  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>

<body class="hold-transition skin-blue sidebar-mini">
  <!-- Site wrapper -->
  <div class="wrapper">

    <?php
    require_once("incld/header_incld.php");
    require_once("incld/menu_incld.php");
    ?>

    <!-- =============================================== -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->

      <div class="row">
        <div class="col-md-4">
          <section class="content-header">
            <h1>New Offer</h1>
            <ol class="breadcrumb horizontal">
              <li><a href="<?= base_url("html") ?>/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
              <li>Masters</li>
              <li><a href="<?= base_url("html") ?>/offers">Offer</a></li>
              <li class="active">New</li>
            </ol>
          </section>
        </div>
        <div class="col-md-8">
          <ol class="right_btn">
            <li><a href="<?= base_url("html") ?>/recipes" class="btn btn-block btn-primary"><i class="fa fa-fw fa-paper-plane-o"></i> Publish</a></li>
          </ol>
        </div>
      </div>

      <section class="content">
        <div class="row">
          <div class="col-md-12">
            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title"> Offer Details</h3>
              </div>
              <div class="box-body">
                <div class="row">
                  <div class="col-xs-6">
                    <div class="form-group required">
                      <label>Offer Name</label>
                      <input type="text" class="form-control" placeholder="Recipe Name">
                      <span class="help-block">This is required</span>
                    </div>
                  </div>

                  <div class="col-xs-6">
                    <div class="form-group">
                      <label>Select Type</label>
                      <select id="selectType" multiple="multiple">
                        <option>Mobile App</option>
                        <option>Online delivery</option>
                        <option>Cash on delivery</option>
                      </select>
                    </div>
                  </div>

                  <div class="col-xs-6">
                    <div class="form-group">
                    <label>Rule Type</label>
                      <select class="form-control select2" style="width: 100%;">
                        <option> Quantity / Category </option>
                        <option> Dependent / Conditional </option>
                      </select>
                    </div>
                  </div>

                  <div class="col-md-3">
                    <div class="form-group required">
                      <label>Start Date</label>
                      <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" class="form-control pull-right" id="dStart">
                      </div>
                      <!-- /.input group -->
                    </div>
                  </div>

                  <div class="col-md-3">
                    <div class="form-group required">
                      <label>End Date</label>
                      <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" class="form-control pull-right" id="dEnd">
                      </div>
                      <!-- /.input group -->
                    </div>
                  </div>



                </div>
              </div>
            </div>

          </div>


        </div>

        <div class="row">
          <div class="col-md-12">
            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title"> Rule Details</h3>
              </div>
              <div class="box-body">
                <div class="row">
            
                  <div class="col-xs-3">
                    <div class="form-group">
                    <label>Apply To</label>
                      <select class="form-control select2" style="width: 100%;">
                      <option>All Menus </option>
                        <option> All Categories </option>
                        <option> Select Menu </option>
                        <option> Select Category </option>
                      </select>
                    </div>
                  </div>

                  <div class="col-xs-3">
                    <div class="form-group">
                    <label>Adjustment Type </label>
                      <select class="form-control select2" style="width: 100%;">
                        <option>Percentage Discount</option>
                        <option> Price Discount </option>
                        <option> Combo Discount </option>
                      </select>
                    </div>
                  </div>

                  <div class="col-xs-3">
                    <div class="form-group required">
                      <label>Value</label>
                      <input type="text" class="form-control" placeholder="50">
                      <span class="help-block">This is required</span>
                    </div>
                  </div>

                </div>
              </div>
            </div>

          </div>


        </div>

        <div class="row">
          <div class="col-md-12">
            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title"> Rule Details</h3>
              </div>
              <div class="box-body">
                <div class="row">
            
                  <div class="col-xs-3">
                    <div class="form-group">
                    <label>Apply To</label>
                      <select class="form-control select2" style="width: 100%;">
                      <option>All Menus </option>
                        <option> All Categories </option>
                        <option> Select Menu </option>
                        <option selected="selected"> Select Category </option>
                      </select>
                    </div>
                  </div>
                  
                  <div class="col-xs-3">
                    <div class="form-group">
                    <label>Menu Category</label>
                      <select class="form-control select2" style="width: 100%;">
                        <option>Starter </option>
                        <option selected="selected"> Main Cource </option>
              
                      </select>
                    </div>
                  </div>



                  <div class="col-xs-3">
                    <div class="form-group">
                    <label>Adjustment Type </label>
                      <select class="form-control select2" style="width: 100%;">
                        <option>Percentage Discount</option>
                        <option> Price Discount </option>
                        <option> Combo Discount </option>
                      </select>
                    </div>
                  </div>

                  <div class="col-xs-3">
                    <div class="form-group required">
                      <label>Value</label>
                      <input type="text" class="form-control" placeholder="50">
                      <span class="help-block">This is required</span>
                    </div>
                  </div>

                </div>
              </div>
            </div>

          </div>


        </div>

        

        <div class="row">
          <div class="col-md-12">
            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title"> Rule Details</h3>
              </div>
              <div class="box-body">
                <div class="row">
            
                  <div class="col-xs-3">
                    <div class="form-group">
                    <label>Apply To</label>
                      <select class="form-control select2" style="width: 100%;">
                        <option>All Menus </option>
                        <option> All Categories </option>
                        <option selected="selected"> Select  Menu </option>
                        <option> Select Category </option>
                      </select>
                    </div>
                  </div>

                  <div class="col-xs-3">
                    <div class="form-group">
                    <label>Menu Category</label>
                      <select class="form-control select2" style="width: 100%;">
                        <option>Starter </option>
                        <option selected="selected"> Main Cource </option>
              
                      </select>
                    </div>
                  </div>

                  <div class="col-xs-3">
                    <div class="form-group">
                      <label>Menu </label>
                      <select id="selectmenu" multiple="multiple">
                        <option>Panner Tikka</option>
                        <option>Aloo Bengan</option>
                      </select>
                    </div>
                  </div>


                  


                  <div class="col-xs-3">
                    <div class="form-group">
                    <label>Receive discount for </label>
                      <select class="form-control select2" style="width: 100%;">
                        <option>Same Menu</option>
                        <option> Other Menu </option>
                      </select>
                    </div>
                  </div>


                  <div class="col-xs-3">
                    <div class="form-group">
                      <label>Free Menu </label>
                      <select id="freemenu" multiple="multiple">
                        <option>Panner Tikka</option>
                        <option>Aloo Bengan</option>
                      </select>
                    </div>
                  </div>

                  
                  <div class="col-xs-3">
                    <div class="form-group required">
                      <label>Free Qty</label>
                      <input type="text" class="form-control" placeholder="1">
                      <span class="help-block">This is required</span>
                    </div>
                  </div>

                  <div class="col-xs-3">
                    <div class="form-group">
                    <label>Discount percentage</label>
                      <select class="form-control select2" style="width: 100%;">
                        <option>100% Percentage</option>
                        <option> Limited Percentage </option>
                      </select>
                    </div>
                  </div>

                  <div class="col-xs-3">
                    <div class="form-group required">
                      <label>Value</label>
                      <input type="text" class="form-control" placeholder="50">
                      <span class="help-block">This is required</span>
                    </div>
                  </div>

                </div>
              </div>
            </div>

          </div>


        </div>


        <div class="row">
          <div class="col-md-12">
            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title"> Rule Details</h3>
              </div>
              <div class="box-body">
                <div class="row">
            
                  <div class="col-xs-2">
                    <div class="form-group">
                    <label>Buy</label>
                      <select class="form-control select2" style="width: 100%;">
                      <option>Any</option>
                      <option>Each</option>
                      </select>
                    </div>
                  </div>
                  
                  <div class="col-xs-2">
                    <div class="form-group">
                    <label>Type of</label>
                      <select class="form-control select2" style="width: 100%;">
                        <option>Menu </option>
                        <option>Category </option>
                      </select>
                    </div>
                  </div>



                  <div class="col-xs-4">
                    <div class="form-group">
                    <label>is </label>
                      <select class="form-control select2" style="width: 100%;">
                        <option>More than or equal to</option>
                        <option> Less than or equal to </option>
                        <option> Equal </option>
                      </select>
                    </div>
                  </div>

                  <div class="col-xs-2">
                    <div class="form-group required">
                      <label>Qty</label>
                      <input type="text" class="form-control" placeholder="50">
                      <span class="help-block">This is required</span>
                    </div>
                  </div>


                  <div class="col-xs-2">
                    <div class="form-group required">
                      <label>Value</label>
                      <input type="text" class="form-control" placeholder="50">
                      <span class="help-block">This is required</span>
                    </div>
                  </div>

                </div>
              </div>
            </div>

          </div>


        </div>




      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <?php
    require_once("incld/footer_incld.php");
    require_once("incld/sidebar_incld.php");
    ?>
    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
  </div>
  <!-- ./wrapper -->

  <!-- jQuery 2.2.3 -->
  <script src="<?= base_url("assets/adminlte") ?>/plugins/jQuery/jquery-2.2.3.min.js"></script>
  <!-- Bootstrap 3.3.6 -->
  <script src="<?= base_url("assets/adminlte") ?>/bootstrap/js/bootstrap.min.js"></script>
  <!-- Select2 -->
  <script src="<?= base_url("assets/adminlte") ?>/plugins/select2/select2.full.min.js"></script>
  <!-- SlimScroll -->
  <script src="<?= base_url("assets/adminlte") ?>/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- bootstrap datepicker -->
    <script src="<?= base_url("assets/adminlte") ?>/plugins/datepicker/bootstrap-datepicker.js"></script>
  <!-- FastClick -->
  <script src="<?= base_url("assets/adminlte") ?>/plugins/fastclick/fastclick.js"></script>
  <!-- AdminLTE App -->
  <script src="<?= base_url("assets/adminlte") ?>/dist/js/app.min.js"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="<?= base_url("assets/adminlte") ?>/dist/js/demo.js"></script>
  <!-- page script -->

  <!-- page script -->
  <script src="<?= base_url("assets/adminlte") ?>/dist/js/bootstrap-multiselect.min.js"></script>
  <script>
    $(function() {

      $(".select2").select2({
        placeholder: "Category",
      });

      $('#selectType, #selectmenu, #freemenu, #selectmenu1').multiselect({
        buttonWidth: '100%',
        includeSelectAllOption: true,
        nonSelectedText: 'Select an Option'
      });

      $('#dStart, #dEnd').datepicker({
        autoclose: true
      });

    });
  </script>
</body>

</html>