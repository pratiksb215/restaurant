<?php
  $dashboard_group = ($menu_active == "index" || $menu_active == "index2") ? "active" : "";
  $index_menu = ($menu_active == "index") ? "active" : "";
  $index2_menu = ($menu_active == "index2") ? "active" : "";

  $layout_group = ($menu_active == "top_nav" || $menu_active == "boxed" || $menu_active == "fixed" || $menu_active == "collapsed_sidebar") ? "active" : "";
  $topnav_menu = ($menu_active == "top_nav" ) ? "active" : "";
  $boxed_menu = ($menu_active == "boxed" ) ? "active" : "";
  $fixed_menu = ($menu_active == "fixed" ) ? "active" : "";
  $collapsedsidebar_menu = ($menu_active == "collapsed_sidebar" ) ? "active" : "";

  $widgets_menu = ($menu_active == "widgets" ) ? "active" : "";

  $charts_group = ($menu_active == "chartjs" || $menu_active == "morris" || $menu_active == "flot" || $menu_active == "inline") ? "active" : "";
  $chartjs_menu = ($menu_active == "chartjs" ) ? "active" : "";
  $morris_menu = ($menu_active == "morris" ) ? "active" : "";
  $flot_menu = ($menu_active == "flot" ) ? "active" : "";
  $inline_menu = ($menu_active == "inline" ) ? "active" : "";

  $uielement_group = ($menu_active == "general" || $menu_active == "icons" || $menu_active == "buttons" || $menu_active == "sliders" || $menu_active == "timeline" || $menu_active == "modals") ? "active" : "";
  $general_menu = ($menu_active == "general" ) ? "active" : "";
  $icons_menu = ($menu_active == "icons" ) ? "active" : "";
  $buttons_menu = ($menu_active == "buttons" ) ? "active" : "";
  $sliders_menu = ($menu_active == "sliders" ) ? "active" : "";
  $timeline_menu = ($menu_active == "timeline" ) ? "active" : "";
  $modals_menu = ($menu_active == "modals" ) ? "active" : "";

  $forms_group = ($menu_active == "general_form" || $menu_active == "advanced" || $menu_active == "editors") ? "active" : "";
  $generalform_menu = ($menu_active == "general_form" ) ? "active" : "";
  $advanced_menu = ($menu_active == "advanced" ) ? "active" : "";
  $editors_menu = ($menu_active == "editors" ) ? "active" : "";

  $tables_group = ($menu_active == "simple" || $menu_active == "data") ? "active" : "";
  $simple_menu = ($menu_active == "simple" ) ? "active" : "";
  $data_menu = ($menu_active == "data" ) ? "active" : "";

  $calendar_menu = ($menu_active == "calendar" ) ? "active" : "";

  $mailbox_group = ($menu_active == "inbox" || $menu_active == "compose" || $menu_active == "read") ? "active" : "";
  $inbox_menu = ($menu_active == "inbox" ) ? "active" : "";
  $compose_menu = ($menu_active == "compose" ) ? "active" : "";
  $read_menu = ($menu_active == "read" ) ? "active" : "";

  $examples_group = ($menu_active == "invoice" || $menu_active == "profile" || $menu_active == "login" || $menu_active == "register" || $menu_active == "lockscreen" || $menu_active == "error404" || $menu_active == "error500" || $menu_active == "blank" || $menu_active == "pace") ? "active" : "";
  $invoice_menu = ($menu_active == "invoice" ) ? "active" : "";
  $profile_menu = ($menu_active == "profile" ) ? "active" : "";
  $login_menu = ($menu_active == "login" ) ? "active" : "";
  $register_menu = ($menu_active == "register" ) ? "active" : "";
  $lockscreen_menu = ($menu_active == "lockscreen" ) ? "active" : "";
  $error404_menu = ($menu_active == "error404" ) ? "active" : "";
  $error500_menu = ($menu_active == "error500" ) ? "active" : "";
  $blank_menu = ($menu_active == "blank" ) ? "active" : "";
  $pace_menu = ($menu_active == "pace" ) ? "active" : "";
?>

<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="<?= base_url("assets/adminlte") ?>/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p>Alexander Pierce <?= $editors_menu ?></p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>
    <!-- search form -->
    <form action="#" method="get" class="sidebar-form">
      <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Search...">
            <span class="input-group-btn">
              <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
              </button>
            </span>
      </div>
    </form>
    <!-- /.search form -->
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
      <li class="header">MAIN NAVIGATION</li>
      <li class="<?= $dashboard_group ?> treeview">
        <a href="#">
          <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li class="<?= $index_menu ?>">
            <a href="<?= base_url("adminlte") ?>"><i class="fa fa-circle-o"></i> Dashboard v1</a>
          </li>
          <li class="<?= $index2_menu ?>">
            <a href="<?= base_url("adminlte") ?>/index2"><i class="fa fa-circle-o"></i> Dashboard v2</a>
          </li>
        </ul>
      </li>
      <li class="<?= $layout_group ?> treeview">
        <a href="#">
          <i class="fa fa-files-o"></i>
          <span>Layout Options</span>
          <span class="pull-right-container">
            <span class="label label-primary pull-right">4</span>
          </span>
        </a>
        <ul class="treeview-menu">
          <li class="<?= $topnav_menu ?>">
            <a href="<?= base_url("adminlte") ?>/top_nav"><i class="fa fa-circle-o"></i> Top Navigation</a>
          </li>
          <li class="<?= $boxed_menu ?>">
            <a href="<?= base_url("adminlte") ?>/boxed"><i class="fa fa-circle-o"></i> Boxed</a>
          </li>
          <li class="<?= $fixed_menu ?>">
            <a href="<?= base_url("adminlte") ?>/fixed"><i class="fa fa-circle-o"></i> Fixed</a>
          </li>
          <li class="<?= $collapsedsidebar_menu ?>">
            <a href="<?= base_url("adminlte") ?>/collapsed_sidebar"><i class="fa fa-circle-o"></i> Collapsed Sidebar</a>
          </li>
        </ul>
      </li>
      <li class="<?= $widgets_menu ?>">
        <a href="<?= base_url("adminlte") ?>/widgets">
          <i class="fa fa-th"></i> <span>Widgets</span>
          <span class="pull-right-container">
            <small class="label pull-right bg-green">new</small>
          </span>
        </a>
      </li>
      <li class="<?= $charts_group ?> treeview">
        <a href="#">
          <i class="fa fa-pie-chart"></i>
          <span>Charts</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li class="<?= $chartjs_menu ?>">
            <a href="<?= base_url("adminlte") ?>/chartjs"><i class="fa fa-circle-o"></i> ChartJS</a>
          </li>
          <li class="<?= $morris_menu ?>">
            <a href="<?= base_url("adminlte") ?>/morris"><i class="fa fa-circle-o"></i> Morris</a>
          </li>
          <li class="<?= $flot_menu ?>">
            <a href="<?= base_url("adminlte") ?>/flot"><i class="fa fa-circle-o"></i> Flot</a>
          </li class="<?= $chartjs_menu ?>">
          <li class="<?= $inline_menu ?>">
            <a href="<?= base_url("adminlte") ?>/inline"><i class="fa fa-circle-o"></i> Inline charts</a>
          </li>
        </ul>
      </li>
      <li class="<?= $uielement_group ?> treeview">
        <a href="#">
          <i class="fa fa-laptop"></i>
          <span>UI Elements</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li class="<?= $general_menu ?>">
            <a href="<?= base_url("adminlte") ?>/general"><i class="fa fa-circle-o"></i> General</a>
          </li>
          <li class="<?= $icons_menu ?>">
            <a href="<?= base_url("adminlte") ?>/icons"><i class="fa fa-circle-o"></i> Icons</a>
          </li>
          <li class="<?= $buttons_menu ?>">
            <a href="<?= base_url("adminlte") ?>/buttons"><i class="fa fa-circle-o"></i> Buttons</a>
          </li>
          <li class="<?= $sliders_menu ?>">
            <a href="<?= base_url("adminlte") ?>/sliders"><i class="fa fa-circle-o"></i> Sliders</a>
          </li>
          <li class="<?= $timeline_menu ?>">
            <a href="<?= base_url("adminlte") ?>/timeline"><i class="fa fa-circle-o"></i> Timeline</a>
          </li>
          <li class="<?= $modals_menu ?>">
            <a href="<?= base_url("adminlte") ?>/modals"><i class="fa fa-circle-o"></i> Modals</a>
          </li>
        </ul>
      </li>
      <li class="<?= $forms_group ?> treeview">
        <a href="#">
          <i class="fa fa-edit"></i> <span>Forms</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li class="<?= $generalform_menu ?>">
            <a href="<?= base_url("adminlte") ?>/general_form"><i class="fa fa-circle-o"></i> General Elements</a>
          </li>
          <li class="<?= $advanced_menu ?>">
            <a href="<?= base_url("adminlte") ?>/advanced"><i class="fa fa-circle-o"></i> Advanced Elements</a>
          </li>
          <li class="<?= $editors_menu ?>">
            <a href="<?= base_url("adminlte") ?>/editors"><i class="fa fa-circle-o"></i> Editors</a>
          </li>
        </ul>
      </li>
      <li class="<?= $tables_group ?> treeview">
        <a href="#">
          <i class="fa fa-table"></i> <span>Tables</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li class="<?= $simple_menu ?>">
            <a href="<?= base_url("adminlte") ?>/simple"><i class="fa fa-circle-o"></i> Simple tables</a>
          </li>
          <li class="<?= $data_menu ?>">
            <a href="<?= base_url("adminlte") ?>/data"><i class="fa fa-circle-o"></i> Data tables</a>
          </li>
        </ul>
      </li>
      <li class="<?= $calendar_menu ?>">
        <a href="<?= base_url("adminlte") ?>/calendar">
          <i class="fa fa-calendar"></i> <span>Calendar</span>
          <span class="pull-right-container">
            <small class="label pull-right bg-red">3</small>
            <small class="label pull-right bg-blue">17</small>
          </span>
        </a>
      </li>
      <li class="<?= $mailbox_group ?> treeview">
        <a href="<?= base_url("adminlte") ?>/mailbox">
          <i class="fa fa-envelope"></i> <span>Mailbox</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li class="<?= $inbox_menu ?>">
            <a href="<?= base_url("adminlte") ?>/inbox"><i class="fa fa-circle-o"></i> Inbox</a>
          </li>
          <li class="<?= $compose_menu ?>">
            <a href="<?= base_url("adminlte") ?>/compose"><i class="fa fa-circle-o"></i> Compose</a>
          </li>
          <li class="<?= $read_menu ?>">
            <a href="<?= base_url("adminlte") ?>/read"><i class="fa fa-circle-o"></i> Read</a>
          </li>
        </ul>
      </li>
      <li class="<?= $examples_group ?> treeview">
        <a href="#">
          <i class="fa fa-folder"></i> <span>Examples</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li class="<?= $invoice_menu ?>">
            <a href="<?= base_url("adminlte") ?>/invoice"><i class="fa fa-circle-o"></i> Invoice</a>
          </li>
          <li class="<?= $profile_menu ?>">
            <a href="<?= base_url("adminlte") ?>/profile"><i class="fa fa-circle-o"></i> Profile</a>
          </li>
          <li class="<?= $login_menu ?>">
            <a href="<?= base_url("adminlte") ?>/login" target="_blank"><i class="fa fa-circle-o"></i> Login</a>
          </li>
          <li class="<?= $register_menu ?>">
            <a href="<?= base_url("adminlte") ?>/register" target="_blank"><i class="fa fa-circle-o"></i> Register</a>
          </li>
          <li class="<?= $lockscreen_menu ?>">
            <a href="<?= base_url("adminlte") ?>/lockscreen" target="_blank"><i class="fa fa-circle-o"></i> Lockscreen</a>
          </li>
          <li class="<?= $error404_menu ?>">
            <a href="<?= base_url("adminlte") ?>/error404"><i class="fa fa-circle-o"></i> 404 Error</a>
          </li>
          <li class="<?= $error500_menu ?>">
            <a href="<?= base_url("adminlte") ?>/error500"><i class="fa fa-circle-o"></i> 500 Error</a>
          </li>
          <li class="<?= $blank_menu ?>">
            <a href="<?= base_url("adminlte") ?>/blank"><i class="fa fa-circle-o"></i> Blank Page</a>
          </li>
          <li class="<?= $pace_menu ?>">
            <a href="<?= base_url("adminlte") ?>/pace"><i class="fa fa-circle-o"></i> Pace Page</a>
          </li>
        </ul>
      </li>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-share"></i> <span>Multilevel</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
          <li>
            <a href="#"><i class="fa fa-circle-o"></i> Level One
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="#"><i class="fa fa-circle-o"></i> Level Two</a></li>
              <li>
                <a href="#"><i class="fa fa-circle-o"></i> Level Two
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                  <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                </ul>
              </li>
            </ul>
          </li>
          <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
        </ul>
      </li>
      <li><a href="<?= base_url("adminlte") ?>/documentation" target="_blank"><i class="fa fa-book"></i> <span>Documentation</span></a></li>
      <li class="header">LABELS</li>
      <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
      <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
      <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li>
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>