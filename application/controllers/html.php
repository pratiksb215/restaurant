<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Html extends CI_Controller {

	public function index()
	{
		$menu_active = "index";
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('html/login', $data);
	}

	public function forgot_password_email()
	{
		$menu_active = "index";
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('html/forgot_password_email', $data);
	}

	public function forgot_password()
	{
		$menu_active = "index";
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('html/forgot_password', $data);
	}

	public function dashboard()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('html/dashboard', $data);
	}
	public function place_order()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('html/place-order', $data);
	}

	public function place_order_admin()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('html/place-order-admin', $data);
	}

	public function kitchen()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('html/kitchen', $data);
	}

	public function bar()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('html/bar', $data);
	}



	public function ingredient_categories()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('html/ingredient_categories', $data);
	}
	public function ingredient_units()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('html/ingredient_units', $data);
	}
	public function ingredients()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('html/ingredients', $data);
	}
	public function liquor()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('html/liquor', $data);
	}
	public function vats()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('html/vats', $data);
	}
	public function recipes_categories()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('html/recipes_categories', $data);
	}
	public function recipes()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('html/recipes', $data);
	}
	public function new_recipe()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('html/new_recipe', $data);
	}
	public function menus()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('html/menus', $data);
	}
	public function suppliers()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('html/suppliers', $data);
	}
	public function customers()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('html/customers', $data);
	}
	public function expense_items()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('html/expense_items', $data);
	}
	public function payment_methods()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('html/payment_methods', $data);
	}
	public function waiters()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('html/waiters', $data);
	}
	public function tables()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('html/tables', $data);
	}
	public function offers()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('html/offers', $data);
	}
	public function new_offer()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('html/new_offer', $data);
	}
	public function rewards_points()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('html/rewards_points', $data);
	}

	public function add_rewards_points()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('html/add_rewards_points', $data);
	}

	public function outlets()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('html/outlets', $data);
	}

	public function users()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('html/users', $data);
	}
	
	public function attendance()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('html/attendance', $data);
	}

	

	public function expenses()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('html/expenses', $data);
	}

	public function settings()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('html/settings', $data);
	}

	public function profile()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('html/profile', $data);
	}

	public function purchases()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('html/purchases', $data);
	}


	public function additional_charges()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('html/additional-charges', $data);
	}

	public function new_purchase()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('html/new_purchase', $data);
	}
	public function purchase_details()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('html/purchase_details', $data);
	}

	public function inventory()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('html/inventory', $data);
	}
	public function inventory_alert()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('html/inventory_alert', $data);
	}


	public function inventory_adjustment()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('html/inventory_adjustment', $data);
	}

	public function new_adjustment()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('html/new_adjustment', $data);
	}

	public function adjustment_details()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('html/adjustment_details', $data);
	}


	public function invoice()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('html/invoice', $data);
	}

	public function order_receipt()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('html/order_receipt', $data);
	}


	public function report_sales()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('html/report-sales', $data);
	}

	public function report_raw()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('html/report-raw-items', $data);
	}

	public function supplier_report()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('html/report-supplier', $data);
	}

	public function report_expense()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('html/report-expense', $data);
	}

	public function report_purchase()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('html/report-purchase', $data);
	}

	public function report_attendance()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('html/report-attendance', $data);
	}

	public function base()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('html/base', $data);
	}


	

}
