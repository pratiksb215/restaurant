<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminlte extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function index()
	{
		$menu_active = "index";
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('adminlte/index_vw', $data);
	}
	

	public function index2()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('adminlte/index2_vw', $data);
	}	

	public function top_nav()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('adminlte/top-nav_vw', $data);
	}

	public function boxed()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('adminlte/boxed_vw', $data);
	}

	public function fixed()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('adminlte/fixed_vw', $data);
	}

	public function collapsed_sidebar()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('adminlte/collapsed-sidebar_vw', $data);
	}

	public function widgets()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('adminlte/widgets_vw', $data);
	}

	public function chartjs()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('adminlte/chartjs_vw', $data);
	}

	public function morris()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('adminlte/morris_vw', $data);
	}

	public function flot()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('adminlte/flot_vw', $data);
	}

	public function inline()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('adminlte/inline_vw', $data);
	}

	public function general()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('adminlte/general_vw', $data);
	}

	public function icons()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('adminlte/icons_vw', $data);
	}

	public function buttons()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('adminlte/buttons_vw', $data);
	}

	public function sliders()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('adminlte/sliders_vw', $data);
	}

	public function timeline()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('adminlte/timeline_vw', $data);
	}

	public function modals()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('adminlte/modals_vw', $data);
	}

	public function general_form()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('adminlte/general-form_vw', $data);
	}

	public function advanced()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('adminlte/advanced_vw', $data);
	}

	public function editors()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('adminlte/editors_vw', $data);
	}

	public function simple()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('adminlte/simple_vw', $data);
	}

	public function data()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('adminlte/data_vw', $data);
	}	

	public function calendar()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('adminlte/calendar_vw', $data);
	}

	public function inbox()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('adminlte/mailbox_vw', $data);
	}

	public function compose()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('adminlte/compose_vw', $data);
	}

	public function read()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('adminlte/read-mail_vw', $data);
	}

	public function invoice()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('adminlte/invoice_vw', $data);
	}

	public function invoice_print()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => "invoice"
			);
		$this->load->view('adminlte/invoice-print_vw', $data);
	}

	public function profile()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('adminlte/profile_vw', $data);
	}

	public function login()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('adminlte/login_vw', $data);
	}

	public function register()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('adminlte/register_vw', $data);
	}

	public function lockscreen()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('adminlte/lockscreen_vw', $data);
	}

	public function error404()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('adminlte/404_vw', $data);
	}

	public function error500()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('adminlte/500_vw', $data);
	}

	public function blank()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('adminlte/blank_vw', $data);
	}

	public function pace()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('adminlte/pace_vw', $data);
	}

	public function documentation()
	{
		$menu_active = $this->uri->segment(2);
		$data = array(
				'menu_active' => $menu_active
			);
		$this->load->view('adminlte/documentation_vw', $data);
	}
	

}
